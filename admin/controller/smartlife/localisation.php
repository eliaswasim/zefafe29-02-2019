<?php

/**
* @version [Supported opencart version 3.x.x.x.]
* @category SmartLife
* @package Opencart Smart Life ERP System Connector
* @author  Muhammed Abdul Wahab Shawa | Smart Life 
* @copyright Copyright (c) 2010-2019 Smart Life Software Private Limited 
*/

class ControllerSmartlifeLocalisation extends Controller {
	private $error = array();
    private $count_tasks = 6 ; 
	public function index() {
         
        $this->registry->set('sl',new Smartlife($this->registry));
        
        $data = array();
        $data = array_merge($data, $this->load->language('extension/module/smartlife'));
		$this->document->setTitle($this->language->get('heading_title_localisation'));
        
        $data['total'] = $this->count_tasks;
        $data['token'] = 'user_token=' . $this->session->data['user_token']; 
		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}
       
		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'user_token=' . $this->session->data['user_token'], true)
		);

 

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('smartlife/localisation', 'user_token=' . $this->session->data['user_token'], true)
		);
 
        $data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('smartlife/localisation', $data));
	}

   
    public function synchronization(){
        
        $this->registry->set('sl',new Smartlife($this->registry));
        $this->load->language('extension/module/smartlife');
		$this->load->model('smartlife/localisation') ;		
		
        $json = array();
        
        $count_tasks = $this->count_tasks ;
        
		if (isset($this->request->get['step'])) {
			$step = $this->request->get['step'];
		} else {
			$step = 1;
		}
        
		// Synchronize Tax Rates
		if ($step == 1) {
			 
			try {
			    $taxes = $this->sl->getTaxes();
                $this->model_smartlife_localisation->SynchronizeTaxRates($taxes); 
				$json['success'] = sprintf($this->language->get('text_progress'), $this->language->get('text_synchronize_taxes'), $step, $count_tasks);
			
				$json['next'] = str_replace('&amp;', '&', $this->url->link('smartlife/localisation/synchronization', 'step=' . ($step + 1) . '&user_token=' . $this->session->data['user_token'] ));
			} catch(Exception $exception) {
				$json['error'] = sprintf($this->language->get('error_exception'), $exception->getCode(), $exception->getMessage(), $exception->getFile(), $exception->getLine());
			}		
		}
        elseif ($step == 2) {
			 
			try {
                $currencies = $this->sl->getCurrencies();
                $this->model_smartlife_localisation->SynchronizeCurrencies($currencies); 
				$json['success'] = sprintf($this->language->get('text_progress'), $this->language->get('text_synchronize_currency'), $step, $count_tasks);
			
				$json['next'] = str_replace('&amp;', '&', $this->url->link('smartlife/localisation/synchronization', 'step=' . ($step + 1) . '&user_token=' . $this->session->data['user_token'] ));
			} catch(Exception $exception) {
				$json['error'] = sprintf($this->language->get('error_exception'), $exception->getCode(), $exception->getMessage(), $exception->getFile(), $exception->getLine());
			}		
		} 
        elseif ($step == 3) {
			 
			try {
                $categories = $this->sl->getCategories();
                $this->model_smartlife_localisation->SynchronizeCategories($categories); 
				$json['success'] = sprintf($this->language->get('text_progress'), $this->language->get('text_synchronize_categories'), $step, $count_tasks);
			
				$json['next'] = str_replace('&amp;', '&', $this->url->link('smartlife/localisation/synchronization', 'step=' . ($step + 1) . '&user_token=' . $this->session->data['user_token'] ));
			} catch(Exception $exception) {
				$json['error'] = sprintf($this->language->get('error_exception'), $exception->getCode(), $exception->getMessage(), $exception->getFile(), $exception->getLine());
			}		
		}
        elseif ($step == 4) {
			 
			try {
                $SubCategories = $this->sl->getSubCategories();
                $this->model_smartlife_localisation->SynchronizeSubCategories($SubCategories); 
				$json['success'] = sprintf($this->language->get('text_progress'), $this->language->get('text_synchronize_sub_categories'), $step, $count_tasks);
			
				$json['next'] = str_replace('&amp;', '&', $this->url->link('smartlife/localisation/synchronization', 'step=' . ($step + 1) . '&user_token=' . $this->session->data['user_token'] ));
			} catch(Exception $exception) {
				$json['error'] = sprintf($this->language->get('error_exception'), $exception->getCode(), $exception->getMessage(), $exception->getFile(), $exception->getLine());
			}		
		}
        elseif ($step == 5) {
			 
			try {
			    $brands = $this->sl->getBrands();
                $this->model_smartlife_localisation->SynchronizeBrands($brands); 
				$json['success'] = sprintf($this->language->get('text_progress'), $this->language->get('text_synchronize_manufacturers'), $step, $count_tasks);
			    $json['next'] = str_replace('&amp;', '&', $this->url->link('smartlife/localisation/synchronization', 'step=' . ($step + 1) . '&user_token=' . $this->session->data['user_token'] ));
			} catch(Exception $exception) {
				$json['error'] = sprintf($this->language->get('error_exception'), $exception->getCode(), $exception->getMessage(), $exception->getFile(), $exception->getLine());
			}		
		}
        elseif ($step == 6) {
			 
			try {
			    $variants = $this->sl->getVariants();
                $this->model_smartlife_localisation->SynchronizeOptions($variants); 
				$json['success'] = sprintf($this->language->get('text_progress'), $this->language->get('text_synchronize_options'), $step, $count_tasks);
			    $json['next'] = str_replace('&amp;', '&', $this->url->link('smartlife/localisation/synchronization', 'step=' . ($step + 1) . '&user_token=' . $this->session->data['user_token'] ));
			} catch(Exception $exception) {
				$json['error'] = sprintf($this->language->get('error_exception'), $exception->getCode(), $exception->getMessage(), $exception->getFile(), $exception->getLine());
			}		
		}
        
        
        else {
			$json['success'] = $this->language->get('text_success');
		}
				
		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));		
    }
 
} 
