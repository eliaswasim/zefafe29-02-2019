<?php

/**
* @version [Supported opencart version 3.x.x.x.]
* @category SmartLife
* @package Opencart Smart Life ERP System Connector
* @author  Muhammed Abdul Wahab Shawa | Smart Life 
* @copyright Copyright (c) 2010-2019 Smart Life Software Private Limited 
*/

class ControllerSmartlifeRecent extends Controller {
	private $error = array();
    private $pages ; 
    private $limit = 1 ; 
	public function index() {
	   
        $this->load->model('smartlife/products') ; 
        $this->registry->set('sl',new Smartlife($this->registry));
        $whid = (int) $this->config->get('module_smartlife_warehouse');
        $LastModified = $this->model_smartlife_products->getLastModified(); 
        $LastModified = '2019-04-20 11:35:33'; //date('Y-m-d H:i:s',strtotime('-6 hour',strtotime($LastModified)));
         
        $count_products    =  (int) $this->sl->getRecentProductsCount($whid,$LastModified); 
        
        
        
        $this->pages = ceil($count_products / $this->limit);
       
        $data = array();
        $data = array_merge($data, $this->load->language('extension/module/smartlife'));
		$this->document->setTitle($this->language->get('heading_title_products'));
        
        $data['total'] = $this->pages;
        $data['total_products'] = $count_products;
        
        $data['token'] = 'user_token=' . $this->session->data['user_token']; 
		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}
       
		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'user_token=' . $this->session->data['user_token'], true)
		);

 

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('smartlife/recent', 'user_token=' . $this->session->data['user_token'], true)
		);
 
        $data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('smartlife/recent', $data));
	}

   
    public function synchronization(){
        $this->load->language('extension/module/smartlife');
		$this->load->model('smartlife/products') ;
        
        $this->registry->set('sl',new Smartlife($this->registry));
        $whid = (int) $this->config->get('module_smartlife_warehouse');
        $LastModified = $this->model_smartlife_products->getLastModified(); 
        $LastModified = '2019-04-20 11:35:33'; //date('Y-m-d H:i:s',strtotime('-6 hour',strtotime($LastModified)));
        $count_products    =  (int) $this->sl->getRecentProductsCount($whid,$LastModified); 
        $this->pages = ceil($count_products / $this->limit);
         		
		
        $json = array();
        
        if (isset($this->request->get['step'])) {
			$step = $this->request->get['step'];
		} else {
			$step = 1;
		}
        
         $offset = ($step - 1)  * $this->limit;
          
		 
		if ($step <= $this->pages) {
			 
			try {
			    
         
			    $products = $this->sl->getProducts($whid,$offset,$this->limit,$LastModified);
                $this->model_smartlife_products->SynchronizeProducts($products);  
				$json['success'] = sprintf($this->language->get('text_progress'), $this->language->get('text_synchronize_products'), $offset, $count_products);
			
				$json['next'] = str_replace('&amp;', '&', $this->url->link('smartlife/recent/synchronization', 'step=' . ($step + 1) . '&user_token=' . $this->session->data['user_token'] ));
			} catch(Exception $exception) {
				$json['error'] = sprintf($this->language->get('error_exception'), $exception->getCode(), $exception->getMessage(), $exception->getFile(), $exception->getLine());
			}		
		}
        
        else {
			$json['success'] = $this->language->get('text_success');
		}
				
		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));		
    }
 
} 
