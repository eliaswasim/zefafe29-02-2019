<?php

/**
* @version [Supported opencart version 3.x.x.x.]
* @category SmartLife
* @package Opencart Smart Life ERP System Connector
* @author  Muhammed Abdul Wahab Shawa | Smart Life 
* @copyright Copyright (c) 2010-2019 Smart Life Software Private Limited 
*/

class ControllerSmartlifeCron extends Controller {
	private $error = array();
    private $pages ; 
    private $limit = 1000 ; 
	public function index() {
         
         
	}

   
    public function synchronization(){
        
        $this->registry->set('sl',new Smartlife($this->registry));
        $whid = (int) $this->config->get('module_smartlife_warehouse');
        $count_products    = (int) $this->sl->getCountProducts($whid); 
        
        $this->pages = ceil($count_products / $this->limit);
        
		$this->load->model('smartlife/products') ;		
		
        $json = array();
        
        if (isset($this->request->get['step'])) {
			$step = $this->request->get['step'];
		} else {
			$step = 1;
		}
        
         $offset = ($step - 1)  * $this->limit;
          
		// Synchronize Tax Rates
		if ($step <= $this->pages) {
			 
			try {
			    $LastModified = $this->model_smartlife_products->getLastModified(); 
			    $products = $this->sl->getProducts($whid,$offset,$this->limit, $LastModified);
                print_r($products);
                $this->model_smartlife_products->SynchronizeProducts($products);  
				$json['success'] = sprintf($this->language->get('text_progress'), $this->language->get('text_synchronize_taxes'), $offset, $count_products);
			
				$json['next'] = str_replace('&amp;', '&', $this->url->link('smartlife/products/synchronization', 'step=' . ($step + 1) . '&user_token=' . $this->session->data['user_token'] ));
			} catch(Exception $exception) {
				$json['error'] = sprintf($this->language->get('error_exception'), $exception->getCode(), $exception->getMessage(), $exception->getFile(), $exception->getLine());
			}		
		}
        
        else {
			$json['success'] = $this->language->get('text_success');
		}
				
		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));		
    }
 
} 
