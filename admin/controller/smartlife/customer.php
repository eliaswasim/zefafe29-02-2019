<?php
/**
* @version [Supported opencart version 3.x.x.x.]
* @category SmartLife
* @package Opencart Smart Life ERP System Connector
* @author  Muhammed Abdul Wahab Shawa | Smart Life 
* @copyright Copyright (c) 2010-2019 Smart Life Software Private Limited 
*/


class ControllerSmartlifeCustomer extends Controller {
	private $error = array();
	private $Language = array();

    public function index() {
    
    }
    
    
    public function sync_customer($route = array(), $request = array() ,$customer_id = null){

	  if (!$customer_id && isset($request[0]) && $request[0]) {
	    $customer_id = $request[0];
	  }

	  $this->load->language('extension/module/smartlife');

	  $this->load->model('smartlife/customer');


	  if(isset($customer_id) && $customer_id){
	    $counter = 0;

	    $customer_array = explode(',', $customer_id);

	    $this->registry->set('sl',new Smartlife($this->registry));

	    $results = $this->model_smartlife_customer->getOcCustomer($customer_array);

	    if($results){
	      $counter = $this->sl->syncCustomer($results);
	    }
	  }
	}
}