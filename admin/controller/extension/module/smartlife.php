<?php

/**
* @version [Supported opencart version 3.x.x.x.]
* @category SmartLife
* @package Opencart Smart Life ERP System Connector
* @author  Muhammed Abdul Wahab Shawa | Smart Life 
* @copyright Copyright (c) 2010-2019 Smart Life Software Private Limited 
*/

class ControllerExtensionModuleSmartlife extends Controller {
    
    private $error = array();
    
    public function index() {
         
        $this->registry->set('sl',new Smartlife($this->registry));
        
        $data = array();
        $data = array_merge($data, $this->load->language('extension/module/smartlife'));
		$this->document->setTitle($this->language->get('heading_title'));
         
        $data['warehouses'] =  $this->sl->getWarehousesList();
		$this->load->model('setting/setting');

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {
			$this->model_setting_setting->editSetting('module_smartlife', $this->request->post);

			$this->session->data['success'] = $this->language->get('text_success');

			$this->response->redirect($this->url->link('extension/module/smartlife', 'user_token=' . $this->session->data['user_token'] . '', true));
		}

		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}
        
        if (isset($this->error['company_code'])) {
			$data['error_company_code'] = $this->error['company_code'];
		} else {
			$data['error_company_code'] = '';
		}
        
        if (isset($this->error['api_key'])) {
			$data['error_api_key'] = $this->error['api_key'];
		} else {
			$data['error_api_key'] = '';
		}
        
		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'user_token=' . $this->session->data['user_token'], true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_extension'),
			'href' => $this->url->link('marketplace/extension', 'user_token=' . $this->session->data['user_token'] . '&type=module', true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('extension/module/smartlife', 'user_token=' . $this->session->data['user_token'], true)
		);

		$data['action'] = $this->url->link('extension/module/smartlife', 'user_token=' . $this->session->data['user_token'], true);

		$data['cancel'] = $this->url->link('marketplace/extension', 'user_token=' . $this->session->data['user_token'] . '&type=module', true);

		if (isset($this->request->post['module_smartlife_status'])) {
			$data['module_smartlife_status'] = $this->request->post['module_smartlife_status'];
		} else {
			$data['module_smartlife_status'] = $this->config->get('module_smartlife_status');
		}
        
        if (isset($this->request->post['module_smartlife_api_key'])) {
			$data['module_smartlife_api_key'] = $this->request->post['module_smartlife_api_key'];
		} else {
			$data['module_smartlife_api_key'] = $this->config->get('module_smartlife_api_key');
		}
        
        if (isset($this->request->post['module_smartlife_company_code'])) {
			$data['module_smartlife_company_code'] = $this->request->post['module_smartlife_company_code'];
		} else {
			$data['module_smartlife_company_code'] = $this->config->get('module_smartlife_company_code');
		}
        
        if (isset($this->request->post['module_smartlife_warehouse'])) {
			$data['module_smartlife_warehouse'] = $this->request->post['module_smartlife_warehouse'];
		} else {
			$data['module_smartlife_warehouse'] = $this->config->get('module_smartlife_warehouse');
		}

		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('extension/module/smartlife/settings', $data));
	}

	protected function validate() {
	   
        $this->load->model('smartlife/smartlife');

        
        
		if (!$this->user->hasPermission('modify', 'extension/module/smartlife')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}
         
        $savedCompany   = $this->config->get('module_smartlife_company_code');
        $company         = $this->request->post['module_smartlife_company_code'];
        
        $savedWarehouse = $this->config->get('module_smartlife_warehouse');
        $warehouse      = $this->request->post['module_smartlife_warehouse'];
        
        if($savedCompany != '' and $company != $savedCompany){
            $this->model_smartlife_smartlife->truncateTables();
        }
        
        if($savedWarehouse != '' and $savedWarehouse != '0' and $warehouse != $savedWarehouse){
            $this->model_smartlife_smartlife->truncateTables();
        }
        
        if ((utf8_strlen($this->request->post['module_smartlife_company_code']) < 3)  ) {
			$this->error['company_code'] = $this->language->get('error_company_code');
		} 
        
        if ((utf8_strlen($this->request->post['module_smartlife_api_key']) < 3) ) {
			$this->error['api_key'] = $this->language->get('error_api_key');
		} 
        

		return !$this->error;
	}
    
    public function adminmenu(){

      $smartlife = array();

      $this->load->language('extension/module/smartlife');

      if ($this->config->get('module_smartlife_status')) {

          if ($this->user->hasPermission('access', 'extension/module/smartlife')) {
              $smartlife[] = array(
                  'name'     => $this->language->get('text_smartlife_settings'),
                  'href'     => $this->url->link('extension/module/smartlife', 'user_token=' . $this->session->data['user_token'], true),
                  'children' => array()
              );
          }
      }
      
      if ($this->config->get('module_smartlife_status') and $this->config->get('module_smartlife_api_key') and $this->config->get('module_smartlife_company_code')) {

          if ($this->user->hasPermission('access', 'smartlife/localisation')) {
              $smartlife[] = array(
                  'name'     => $this->language->get('text_smartlife_localisation'),
                  'href'     => $this->url->link('smartlife/localisation', 'user_token=' . $this->session->data['user_token'], true),
                  'children' => array()
              );
          }
          if ($this->user->hasPermission('access', 'smartlife/products')) {
              $smartlife[] = array(
                  'name'     => $this->language->get('text_smartlife_products'),
                  'href'     => $this->url->link('smartlife/products', 'user_token=' . $this->session->data['user_token'], true),
                  'children' => array()
              );
          }
          
          if ($this->user->hasPermission('access', 'smartlife/recent')) {
              $smartlife[] = array(
                  'name'     => $this->language->get('text_smartlife_recent'),
                  'href'     => $this->url->link('smartlife/recent', 'user_token=' . $this->session->data['user_token'], true),
                  'children' => array()
              );
          }
      }
      
      return $smartlife;
    }
    
    public function install(){

        $this->load->model('smartlife/smartlife');
        $this->load->model('setting/event');

        //$this->model_setting_event->addEvent('module_smartlife', 'catalog/model/account/customer/addCustomer/after','extension/module/smartlife/syncronize_customer');

        //$this->model_setting_event->addEvent('module_smartlife', 'admin/model/customer/customer/addCustomer/after','smartlife/customer/sync_customer');
        $this->model_setting_event->addEvent('module_smartlife', 'catalog/model/checkout/order/addOrderHistory/before','extension/module/smartlife/check_order_status');
        $this->model_setting_event->addEvent('module_smartlife', 'catalog/model/checkout/order/addOrderHistory/after','extension/module/smartlife/syncronize_order');
        $this->model_setting_event->addEvent('module_smartlife', 'catalog/model/checkout/order/deleteOrder/before','extension/module/smartlife/check_before_delete');
        $this->model_smartlife_smartlife->truncateTables();
        $this->model_smartlife_smartlife->AddIdColumn();
    }
    
    public function uninstall(){
        
      $this->load->model('smartlife/smartlife');  
      $this->load->model('setting/event');
      if (!$this->user->hasPermission('modify', 'extension/module/smartlife')) {
           return false; 
      }
      
      $this->model_setting_event->deleteEventByCode('module_smartlife');
      $this->model_smartlife_smartlife->dropIdColumns();
    }

}