<?php

/**
* @version [Supported opencart version 3.x.x.x.]
* @category SmartLife
* @package Opencart Smart Life ERP System Connector
* @author  Muhammed Abdul Wahab Shawa | Smart Life 
* @copyright Copyright (c) 2010-2019 Smart Life Software Private Limited 
*/

$_['heading_title'] 				=       'Smart Life Online Connector - Synchronize Localisation';
$_['heading_title_products'] 	    =       'Smart Life Online Connector - Synchronize Products';

$_['heading_title_localisation'] 				=       'Smart Life Online Connector - Synchronize Localisation';
$_['heading_title_products'] 				=       'Smart Life Online Connector - Synchronize Products';


// Text
$_['text_extension']		=		'Extension';
$_['text_edit']	        	=		'Edit Settings';
$_['text_smartlife_customer']		=		'Customers';
$_['text_smartlife_product']		=		'Products';
$_['text_smartlife_orders']			=		'Orders';
$_['text_smartlife_settings']       = 'Api Settings';
$_['text_smartlife_localisation']   = 'Synchronize Localisation';
$_['text_smartlife_products']   = 'Synchronize Products';
$_['text_smartlife_recent']   = 'Syn Recent Products';

$_['button_synchronize']             = 'Synchronize';
$_['text_progress']   = 'Task %s is applying (%s of %s)';
$_['text_synchronize_taxes'] = 'Synchronize Tax Rates';
$_['text_synchronize_categories'] = 'Synchronize Categories';
$_['text_synchronize_sub_categories'] = 'Synchronize Sub Categories';
$_['text_synchronize_manufacturers'] = 'Synchronize Manufacturers';
$_['text_synchronize_options'] = 'Synchronize Options';
$_['text_synchronize_currency'] = 'Synchronize Currencies';
$_['text_success']       = 'All tasks completed successfully';
$_['text_confirm']       = 'Are you sure you want to change the warehouse? All products and related information will be deleted';
$_['text_confirm_company'] = 'Are you sure you want to change the company? All products and related information will be deleted';
$_['total_products_is'] = 'Total Products Is';
$_['no_products'] = 'No products found to sync';

// entry
$_['entry_status']					=		'Status';
$_['entry_api_key']                 =		'Enter The Api Key';
$_['entry_company_code']            = 'Enter Your Company Code';
$_['entry_warehouse']                = 'Select the warehouse that you want to import products from it  ';
$_['entry_warehouse_option']   = 'Warehouses will appear after entry the correct authentication data and save';
$_['entry_progress']  = 'Progress';
// error

$_['error_company_code'] = 'The Company Code Is Required!';
$_['error_api_key'] = 'The Api Key Is Required!';


