<?php

/**
* @version [Supported opencart version 3.x.x.x.]
* @category SmartLife
* @package Opencart Smart Life ERP System Connector
* @author  Muhammed Abdul Wahab Shawa | Smart Life 
* @copyright Copyright (c) 2010-2019 Smart Life Software Private Limited 
*/

class ModelSmartlifeLocalisation  extends Model {
    
	public function SynchronizeTaxRates($data) {
	    
        $customer_groups_sql =  "SELECT * FROM `" . DB_PREFIX . "customer_group` "; 
        $customer_groups_query = $this->db->query($customer_groups_sql);
        $customer_groups = $customer_groups_query->rows ; 
		
        $this->db->query("DELETE FROM " . DB_PREFIX . "tax_rule ");
        $this->db->query("DELETE FROM " . DB_PREFIX . "tax_rate ");
		$this->db->query("DELETE FROM " . DB_PREFIX . "tax_rate_to_customer_group "); 
         
        foreach ($data as $row){
            
            if($row['type'] == '1'){
                $type = 'P';
            }elseif($row['type'] == '2'){
                 $type = 'F';
            }
            
            $this->db->query("INSERT INTO " . DB_PREFIX . "tax_rate SET smartlife_id = '" . $this->db->escape($row['id']) . "', name = '" . $this->db->escape($row['name']) . "', rate = '" . (float)$row['rate'] . "', `type` = '" . $this->db->escape($type) . "', geo_zone_id = '4', date_added = NOW(), date_modified = NOW()");

    		$tax_rate_id = $this->db->getLastId();
    
    		 
			foreach ($customer_groups as $customer_group_id) {
				$this->db->query("INSERT INTO " . DB_PREFIX . "tax_rate_to_customer_group SET tax_rate_id = '" . (int)$tax_rate_id . "', customer_group_id = '" . (int)$customer_group_id['customer_group_id'] . "'");
			}
    		 
        }
        
		 
	}
    
    public function SynchronizeCategories($data){
        $this->registry->set('sl',new Smartlife($this->registry));
        $this->load->model('localisation/language') ;	
        $languages = $this->model_localisation_language->getLanguages(array());
        
        $this->db->query("DELETE FROM " . DB_PREFIX . "category ");
        $this->db->query("DELETE FROM " . DB_PREFIX . "category_description ");
        $this->db->query("DELETE FROM " . DB_PREFIX . "category_filter ");
        $this->db->query("DELETE FROM " . DB_PREFIX . "category_path ");
        $this->db->query("DELETE FROM " . DB_PREFIX . "category_to_google_product_category ");
        $this->db->query("DELETE FROM " . DB_PREFIX . "category_to_layout ");
        $this->db->query("DELETE FROM " . DB_PREFIX . "category_to_store ");	
        
        foreach ($data as $row){
            
            $this->db->query("INSERT INTO " . DB_PREFIX . "category SET category_id = '".(int)$row['id']."', parent_id = '0', `top` = '1', `column` = '1', sort_order = '1', status = '1', date_modified = NOW(), date_added = NOW()");

		    $category_id = $this->db->getLastId();
            
            if (isset($row['image']) and $row['image'] != '') {
                
                $image = $this->sl->saveImage('https://smarterp.top/assets/uploads/thumbs/'.$row['image'], $category_id , 'categories'); 
    			$this->db->query("UPDATE " . DB_PREFIX . "category SET image = '" . $this->db->escape($image) . "' WHERE category_id = '" . (int)$category_id . "'");
    		}
            
            foreach ($languages as $language){
                $this->db->query("INSERT INTO " . DB_PREFIX . "category_description SET category_id = '" . (int)$category_id . "', language_id = '" . (int)$language['language_id'] . "', name = '" . $this->db->escape($row['name']) . "', description = '', meta_title = '" . $this->db->escape($row['name']) . "', meta_description = '" . $this->db->escape($row['name']) . "', meta_keyword = ''");
            }
            
            $level = 0;
            
            $this->db->query("INSERT INTO `" . DB_PREFIX . "category_path` SET `category_id` = '" . (int)$category_id . "', `path_id` = '" . (int)$category_id . "', `level` = '" . (int)$level . "'");
            $this->db->query("INSERT INTO " . DB_PREFIX . "category_to_store SET category_id = '" . (int)$category_id . "', store_id = '0' ");
            $this->db->query("INSERT INTO " . DB_PREFIX . "category_to_layout SET category_id = '" . (int)$category_id . "', store_id = '0', layout_id = '0'");
        }
        
        $this->cache->delete('category');
     
    }
    
    public function SynchronizeSubCategories($data){
        $this->registry->set('sl',new Smartlife($this->registry));
        $this->load->model('localisation/language') ;	
        $languages = $this->model_localisation_language->getLanguages(array());
         
        $category_id = 0 ; 
        foreach ($data as $row){
            $this->db->query("INSERT INTO " . DB_PREFIX . "category SET category_id = '" . (int)$row['id'] . "', parent_id = '" . (int)$row['parent_id'] . "', `top` = '0', `column` = '1', sort_order = '0', status = '1', date_modified = NOW(), date_added = NOW()");
    
    		$category_id = $this->db->getLastId();
    
    		if (isset($row['image']) and $row['image'] != '') {
                
                $image = $this->sl->saveImage('https://smarterp.top/assets/uploads/thumbs/'.$row['image'], $category_id , 'categories'); 
    			$this->db->query("UPDATE " . DB_PREFIX . "category SET image = '" . $this->db->escape($image) . "' WHERE category_id = '" . (int)$category_id . "'");
    		}
    
    		foreach ($languages as $language){
                $this->db->query("INSERT INTO " . DB_PREFIX . "category_description SET category_id = '" . (int)$category_id . "', language_id = '" . (int)$language['language_id'] . "', name = '" . $this->db->escape($row['name']) . "', description = '', meta_title = '" . $this->db->escape($row['name']) . "', meta_description = '" . $this->db->escape($row['name']) . "', meta_keyword = ''");
            }
    
    		// MySQL Hierarchical Data Closure Table Pattern
    		$level = 0;
    
    		$query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "category_path` WHERE category_id = '" . (int)$row['parent_id'] . "' ORDER BY `level` ASC");
    
    		foreach ($query->rows as $result) {
    			$this->db->query("INSERT INTO `" . DB_PREFIX . "category_path` SET `category_id` = '" . (int)$category_id . "', `path_id` = '" . (int)$result['path_id'] . "', `level` = '" . (int)$level . "'");
    
    			$level++;
    		}
    
    		$this->db->query("INSERT INTO `" . DB_PREFIX . "category_path` SET `category_id` = '" . (int)$category_id . "', `path_id` = '" . (int)$category_id . "', `level` = '" . (int)$level . "'");
            $this->db->query("INSERT INTO " . DB_PREFIX . "category_to_store SET category_id = '" . (int)$category_id . "', store_id = '0' ");
            $this->db->query("INSERT INTO " . DB_PREFIX . "category_to_layout SET category_id = '" . (int)$category_id . "', store_id = '0', layout_id = '0'");
    		 
        }
        
		$this->cache->delete('category');

		return $category_id;
        	
         
     
    }
    
    public function SynchronizeBrands($data){
        $this->registry->set('sl',new Smartlife($this->registry));
         
        $this->db->query("DELETE FROM " . DB_PREFIX . "manufacturer ");
        $this->db->query("DELETE FROM " . DB_PREFIX . "manufacturer_to_store ");
        
        foreach ($data as $row){
            $this->db->query("INSERT INTO " . DB_PREFIX . "manufacturer SET manufacturer_id = '".$row['id']."', name = '" . $this->db->escape($row['name']) . "', sort_order = '0'");

    		$manufacturer_id = $this->db->getLastId();
     
            if (isset($row['image']) and $row['image'] != '') {
                $image = $this->sl->saveImage('https://smarterp.top/assets/uploads/'.$row['image'], $manufacturer_id , 'brands'); 
    			$this->db->query("UPDATE " . DB_PREFIX . "manufacturer SET image = '" . $this->db->escape($image) . "' WHERE manufacturer_id = '" . (int)$manufacturer_id . "'");
    		}
            
    		 
    	 	$this->db->query("INSERT INTO " . DB_PREFIX . "manufacturer_to_store SET manufacturer_id = '" . (int)$manufacturer_id . "', store_id = '0'");
    		 		
        }
        
		
		$this->cache->delete('manufacturer');
    
    }
    
    public function SynchronizeCurrencies($data){
        $this->registry->set('sl',new Smartlife($this->registry));
        $this->load->model('localisation/currency') ;	
        $this->load->model('setting/setting') ;
        $op_currencies = $this->model_localisation_currency->getCurrencies(array());
        
        $allcurrency      = $data['currencies']; 
        $defultcurrency   = $data['defultcurrency']; 
        
        foreach ($allcurrency as $row){
            $code = $row['code'];
            
            if(!isset($op_currencies[$code])){
                
                $this->db->query("INSERT INTO " . DB_PREFIX . "currency SET title = '" . $this->db->escape($row['name']) . "', code = '" . $this->db->escape($row['code']) . "', symbol_left = '" . $this->db->escape($row['symbol']) . "', symbol_right = '', decimal_place = '2', value = '" . $this->db->escape($row['rate']) . "', status = '1', date_modified = NOW()");

        		$currency_id = $this->db->getLastId();
        
        		
            }
        }
         
        $this->model_setting_setting->editSettingValue('config','config_currency',$defultcurrency);
        
        if ($this->config->get('config_currency_auto')) {
			$this->model_localisation_currency->refresh(true);
		}
         
		$this->cache->delete('currency');
        
        
    }
    
    public function SynchronizeOptions($data){
        $this->load->model('localisation/language') ;	
        $languages = $this->model_localisation_language->getLanguages(array());
        
        $this->db->query("DELETE FROM " . DB_PREFIX . "option ");
        $this->db->query("DELETE FROM " . DB_PREFIX . "option_description "); 
        $this->db->query("DELETE FROM " . DB_PREFIX . "option_value ");
        $this->db->query("DELETE FROM " . DB_PREFIX . "option_value_description ");
        
        foreach ($data as $option){
            
            $this->db->query("INSERT INTO `" . DB_PREFIX . "option` SET option_id ='".  $option['id']."', type = 'radio', sort_order = '0'");
            $option_id = $this->db->getLastId();
        }
        
        foreach ($languages as $language){
                $this->db->query("INSERT INTO " . DB_PREFIX . "option_description SET option_id = '" . (int)$option_id . "', language_id = '" . (int)$language['language_id'] . "', name = '" . $this->db->escape($option['name']) . "'");
        }
		 
        foreach ($option['SubVariants'] as $option_value) {
			$this->db->query("INSERT INTO " . DB_PREFIX . "option_value SET option_value_id='".$option_value['id']."', option_id = '" . (int)$option_id . "', image = '', sort_order = '0'");

			$option_value_id = $this->db->getLastId();

			foreach ($languages as $language){
				$this->db->query("INSERT INTO " . DB_PREFIX . "option_value_description SET option_value_id = '" . (int)$option_value_id . "', language_id = '" . (int)$language['language_id']. "', option_id = '" . (int)$option_id . "', name = '" . $this->db->escape($option_value['name']) . "'");
			}
		}
		 
    }
    
}