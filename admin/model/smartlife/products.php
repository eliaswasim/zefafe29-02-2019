<?php

/**
* @version [Supported opencart version 3.x.x.x.]
* @category SmartLife
* @package Opencart Smart Life ERP System Connector
* @author  Muhammed Abdul Wahab Shawa | Smart Life
* @copyright Copyright (c) 2010-2019 Smart Life Software Private Limited
*/

class ModelSmartlifeProducts  extends Model {

	public function SynchronizeProducts($data) {


         foreach($data as $row){
             $check = $this->checkProduct($row);

             if(!$check){
                $this->addProduct($row);
             }
         }
	}

    private function checkProduct($row){
        $this->registry->set('sl',new Smartlife($this->registry));

        $query = $this->db->query("SELECT date_modified FROM " . DB_PREFIX . "product WHERE product_id = '" . (int)$row['id'] . "'");

        if(!isset($query->row['date_modified'])){
            return false;
        }

        $date_modified = $query->row['date_modified'];


        $has_been_modified = $this->sl->compareDates($row['timestamp'] , $date_modified);

        if($has_been_modified){

            $this->updateProduct($row);
        }
        return true;
    }

    private function addProduct($data){

        $this->registry->set('sl',new Smartlife($this->registry));
        $this->load->model('localisation/language') ;
        $languages = $this->model_localisation_language->getLanguages(array());

        $this->db->query("INSERT INTO " . DB_PREFIX . "product SET product_id = '".(int) $data['id']."', model = '" . $this->db->escape($data['code']) . "', sku = '" . $this->db->escape($data['code']) . "', upc = '', ean = '', jan = '', isbn = '', mpn = '', location = '', quantity = '" . (int)$data['quantity'] . "', minimum = '1', subtract = '1', stock_status_id = '5', date_available = '', manufacturer_id = '" . (int)$data['brand'] . "', shipping = '1', price = '" . (float)$data['price'] . "', points = '0', weight = '0.00', weight_class_id = '1', length = '0.00', width = '0.00', height = '0.00', length_class_id = '1', status = '1', tax_class_id = '" . (int)$data['tax_rate'] . "', sort_order = '0', date_added = NOW(), date_modified = NOW()");

		$product_id = $this->db->getLastId();

		if (isset($data['image']) and $data['image']) {
		    $image = $this->sl->saveImage($data['image'], $product_id , 'products');
			$this->db->query("UPDATE " . DB_PREFIX . "product SET image = '" . $image . "' WHERE product_id = '" . (int)$product_id . "'");
		}

        foreach ($languages as $language){
            $this->db->query("INSERT INTO " . DB_PREFIX . "product_description SET product_id = '" . (int)$product_id . "', language_id = '" . (int)$language['language_id'] . "', name = '" . $this->db->escape($data['name']) . "', description = '" . $this->db->escape($data['product_details']) . "', tag = '', meta_title = '" . $this->db->escape($data['name']) . "', meta_description = '" . $this->db->escape($data['name']) . "', meta_keyword = ''");
        }


		$this->db->query("INSERT INTO " . DB_PREFIX . "product_to_store SET product_id = '" . (int)$product_id . "', store_id = '0'");


       if (isset($data['options']) and is_array($data['options'])) {

			foreach ($data['options'] as $product_option) {


					if (isset($product_option['SubVariants']) and is_array($product_option['SubVariants']) and count($product_option['SubVariants']) > 0) {


            $this->db->query("INSERT INTO " . DB_PREFIX . "product_option SET product_id = '" . (int)$product_id . "', option_id = '" . (int)$product_option['id'] . "', required = '1'");

						$product_option_id = $this->db->getLastId();

						foreach ($product_option['SubVariants'] as $product_option_value) {
						  $option_value_id = $this->resolveOptionValue($product_option_value['name']);
							$this->db->query("INSERT INTO " . DB_PREFIX . "product_option_value SET product_option_id = '" . (int)$product_option_id . "', product_id = '" . (int)$product_id . "', option_id = '" . (int)$product_option_value['parent_id'] . "', option_value_id = '" . (int)$option_value_id. "', quantity = '" . (int)$product_option_value['quantity'] . "', subtract = '1', price = '" . (float)$product_option_value['price'] . "', price_prefix = '" .  $product_option_value['price_prefix'] . "', points = '0', points_prefix = '+', weight = '0', weight_prefix = '+'");

						}
					}

			}
		}


		if (isset($data['images']) and count($data['images']) > 0) {
			foreach ($data['images'] as $product_image) {
			    $image = $this->sl->saveImage('https://smarterp.top/assets/uploads/'.$product_image['photo'], $product_id , 'products');
				$this->db->query("INSERT INTO " . DB_PREFIX . "product_image SET product_id = '" . (int)$product_id . "', image = '" . $this->db->escape($image) . "', sort_order = '0'");
			}
		}

		if (isset($data['category_id']) and $data['category_id']) {
		    $this->db->query("INSERT INTO " . DB_PREFIX . "product_to_category SET product_id = '" . (int)$product_id . "', category_id = '" . (int)$data['category_id'] . "'");
	    }

        if (isset($data['subcategory_id']) and $data['subcategory_id']) {
		    $this->db->query("INSERT INTO " . DB_PREFIX . "product_to_category SET product_id = '" . (int)$product_id . "', category_id = '" . (int)$data['subcategory_id'] . "'");
	    }

			if (isset($data['promotion']) and $data['promotion'] == 1) {

					$this->db->query("INSERT INTO " . DB_PREFIX . "product_special SET product_id = '" . (int)$product_id . "', customer_group_id = '1', priority = '0', price = '" . (float)$data['promo_price'] . "', date_start = '" . $this->db->escape($data['start_date']) . "', date_end = '" . $this->db->escape($data['end_date']) . "'");
			}

		$this->db->query("INSERT INTO " . DB_PREFIX . "product_to_layout SET product_id = '" . (int)$product_id . "', store_id = '0', layout_id = '0'");


		$this->cache->delete('product');
    }

    private function updateProduct($data){
       $this->registry->set('sl',new Smartlife($this->registry));
       $this->load->model('localisation/language') ;
       $languages = $this->model_localisation_language->getLanguages(array());

       $product_id = $data['id'];

       $this->db->query("UPDATE " . DB_PREFIX . "product SET model = '" .
       $this->db->escape($data['code']) . "', sku = '', upc = '', ean = '', jan = '', isbn = '', mpn = '', location = '', quantity = '" .
       (int)$data['quantity'] . "', minimum = '1', subtract = '1', stock_status_id = '1', date_available = '1', manufacturer_id = '" . (int)$data['brand'] . "', shipping = '1', price = '" . (float)$data['price']
       . "', points = '0.00', weight = '0.00', weight_class_id = '1', length = '0', width = '0', height = '0', length_class_id = '1', status = '1', tax_class_id = '" . (int)$data['tax_rate'] . "', sort_order = '0', date_modified = NOW() WHERE product_id = '" . (int)$data['id'] . "'");

		if (isset($data['image']) and $data['image']) {
		    $image = $this->sl->saveImage($data['image'], $product_id , 'products');
			$this->db->query("UPDATE " . DB_PREFIX . "product SET image = '" . $this->db->escape($image) . "' WHERE product_id = '" . (int)$product_id . "'");
		}

		$this->db->query("DELETE FROM " . DB_PREFIX . "product_description WHERE product_id = '" . (int)$product_id . "'");

		foreach ($languages as $language){
            $this->db->query("INSERT INTO " . DB_PREFIX . "product_description SET product_id = '" . (int)$product_id . "', language_id = '" . (int)$language['language_id'] . "', name = '" . $this->db->escape($data['name']) . "', description = '" . $this->db->escape($data['product_details']) . "', tag = '', meta_title = '" . $this->db->escape($data['name']) . "', meta_description = '" . $this->db->escape($data['name']) . "', meta_keyword = ''");
        }

		$this->db->query("DELETE FROM " . DB_PREFIX . "product_to_store WHERE product_id = '" . (int)$product_id . "'");

        $this->db->query("INSERT INTO " . DB_PREFIX . "product_to_store SET product_id = '" . (int)$product_id . "', store_id = '0'");

		$this->db->query("DELETE FROM " . DB_PREFIX . "product_option WHERE product_id = '" . (int)$product_id . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "product_option_value WHERE product_id = '" . (int)$product_id . "'");

		if (isset($data['options']) and is_array($data['options'])) {

			foreach ($data['options'] as $product_option) {


					if (isset($product_option['SubVariants']) and is_array($product_option['SubVariants']) and count($product_option['SubVariants']) > 0) {

                        $this->db->query("INSERT INTO " . DB_PREFIX . "product_option SET product_id = '" . (int)$product_id . "', option_id = '" . (int)$product_option['id'] . "', required = '1'");

						$product_option_id = $this->db->getLastId();

						foreach ($product_option['SubVariants'] as $product_option_value) {
							$option_value_id = $this->resolveOptionValue($product_option_value['name']);
							$this->db->query("INSERT INTO " . DB_PREFIX . "product_option_value SET product_option_id = '" . (int)$product_option_id . "', product_id = '" . (int)$product_id . "', option_id = '" . (int)$product_option_value['parent_id'] . "', option_value_id = '" . (int)$option_value_id. "', quantity = '" . (int)$product_option_value['quantity'] . "', subtract = '1', price = '" . (float)$product_option_value['price'] . "', price_prefix = '" .  $product_option_value['price_prefix'] . "', points = '0', points_prefix = '+', weight = '0', weight_prefix = '+'");
						}
					}

			}
		}

		$this->db->query("DELETE FROM " . DB_PREFIX . "product_image WHERE product_id = '" . (int)$product_id . "'");

		if (isset($data['images']) and count($data['images']) > 0) {
			foreach ($data['images'] as $product_image) {
			    $image = $this->sl->saveImage('https://smarterp.top/assets/uploads/'.$product_image['photo'], $product_id , 'products');
				$this->db->query("INSERT INTO " . DB_PREFIX . "product_image SET product_id = '" . (int)$product_id . "', image = '" . $this->db->escape($image) . "', sort_order = '0'");
			}
		}

		$this->db->query("DELETE FROM " . DB_PREFIX . "product_to_category WHERE product_id = '" . (int)$product_id . "'");

		if (isset($data['category_id']) and $data['category_id']) {
		    $this->db->query("INSERT INTO " . DB_PREFIX . "product_to_category SET product_id = '" . (int)$product_id . "', category_id = '" . (int)$data['category_id'] . "'");
	    }

        if (isset($data['subcategory_id']) and $data['subcategory_id']) {
		    $this->db->query("INSERT INTO " . DB_PREFIX . "product_to_category SET product_id = '" . (int)$product_id . "', category_id = '" . (int)$data['subcategory_id'] . "'");
	    }
			$this->db->query("DELETE FROM " . DB_PREFIX . "product_special WHERE product_id = '" . (int)$product_id . "'");
			if (isset($data['promotion']) and $data['promotion'] == 1) {

					$this->db->query("INSERT INTO " . DB_PREFIX . "product_special SET product_id = '" . (int)$product_id . "', customer_group_id = '1', priority = '0', price = '" . (float)$data['promo_price'] . "', date_start = '" . $this->db->escape($data['start_date']) . "', date_end = '" . $this->db->escape($data['end_date']) . "'");
			}

        $this->db->query("DELETE FROM " . DB_PREFIX . "product_to_layout WHERE product_id = '" . (int)$product_id . "'");
		$this->db->query("INSERT INTO " . DB_PREFIX . "product_to_layout SET product_id = '" . (int)$product_id . "', store_id = '0', layout_id = '0'");

        $this->cache->delete('product');
    }

    public function deleteProduct($product_id) {
		$this->db->query("DELETE FROM " . DB_PREFIX . "product WHERE product_id = '" . (int)$product_id . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "product_attribute WHERE product_id = '" . (int)$product_id . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "product_description WHERE product_id = '" . (int)$product_id . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "product_discount WHERE product_id = '" . (int)$product_id . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "product_filter WHERE product_id = '" . (int)$product_id . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "product_image WHERE product_id = '" . (int)$product_id . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "product_option WHERE product_id = '" . (int)$product_id . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "product_option_value WHERE product_id = '" . (int)$product_id . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "product_related WHERE product_id = '" . (int)$product_id . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "product_related WHERE related_id = '" . (int)$product_id . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "product_reward WHERE product_id = '" . (int)$product_id . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "product_special WHERE product_id = '" . (int)$product_id . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "product_to_category WHERE product_id = '" . (int)$product_id . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "product_to_download WHERE product_id = '" . (int)$product_id . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "product_to_layout WHERE product_id = '" . (int)$product_id . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "product_to_store WHERE product_id = '" . (int)$product_id . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "product_recurring WHERE product_id = " . (int)$product_id);
		$this->db->query("DELETE FROM " . DB_PREFIX . "review WHERE product_id = '" . (int)$product_id . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "seo_url WHERE query = 'product_id=" . (int)$product_id . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "coupon_product WHERE product_id = '" . (int)$product_id . "'");

		$this->cache->delete('product');
	}


    public function getLastModified(){
        $query = $this->db->query("SELECT date_modified FROM " . DB_PREFIX . "product ORDER BY date_modified DESC LIMIT 1");

        return $query->row['date_modified'];
    }


		public function resolveOptionValue($value){
			$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "option_value_description WHERE name ='".$value."' ");

			if(isset($query->row['option_value_id']) and $query->row['option_value_id']){
				return $query->row['option_value_id'];
			}

			$insert_option_value = $this->db->query("INSERT INTO ".DB_PREFIX."option_value SET option_id = '1' , sort_order= '0' ");
			$option_value_id = $this->db->getLastId();

			$languages = $this->model_localisation_language->getLanguages(array());
			foreach ($languages as $language){
					$this->db->query("INSERT INTO " . DB_PREFIX . "option_value_description SET option_value_id = '" . (int)$option_value_id . "', language_id = '" . (int)$language['language_id'] . "', name = '" . $this->db->escape($value) . "' , option_id='1' ");
			}

			return $option_value_id;

		}
}
