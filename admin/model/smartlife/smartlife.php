<?php
/**
* @version [Supported opencart version 3.x.x.x.]
* @category SmartLife
* @package Opencart Smart Life ERP System Connector
* @author  Muhammed Abdul Wahab Shawa | Smart Life 
* @copyright Copyright (c) 2010-2019 Smart Life Software Private Limited 
*/
class ModelSmartlifeSmartlife extends model{

	/**
	 * [Trancate Tables On Install Plugin]
	 */
	public function truncateTables($selected_product = 0, $option = null){
	   
        // Truncate Table category
		$this->db->query("TRUNCATE TABLE `".DB_PREFIX ."category` ") ;
        
        // Truncate Table category_description
		$this->db->query("TRUNCATE TABLE `".DB_PREFIX ."category_description` ") ;
        
        // Truncate Table category_filter
		$this->db->query("TRUNCATE TABLE `".DB_PREFIX ."category_filter` ") ;
        
        // Truncate Table category_path
		$this->db->query("TRUNCATE TABLE `".DB_PREFIX ."category_path` ") ;
        
        // Truncate Table category_to_google_product_category
		$this->db->query("TRUNCATE TABLE `".DB_PREFIX ."category_to_google_product_category` ") ;
        
        // Truncate Table category_to_layout
		$this->db->query("TRUNCATE TABLE `".DB_PREFIX ."category_to_layout` ") ;
        
        // Truncate Table category_to_layout
		$this->db->query("TRUNCATE TABLE `".DB_PREFIX ."category_to_layout` ") ;
        
	    // Truncate Table category_to_store
		$this->db->query("TRUNCATE TABLE `".DB_PREFIX ."category_to_store` ") ;
        
        // Truncate Table coupon_category
		$this->db->query("TRUNCATE TABLE `".DB_PREFIX ."coupon_category` ") ;
        
        // Truncate Table coupon_product
		$this->db->query("TRUNCATE TABLE `".DB_PREFIX ."coupon_product` ") ;
        
        // Truncate Table product
		$this->db->query("TRUNCATE TABLE `".DB_PREFIX ."product` ") ;
        
        // Truncate Table product_advertise_google
	    $this->db->query("TRUNCATE TABLE `".DB_PREFIX ."product_advertise_google` ") ;
        
        // Truncate Table product_advertise_google_status
	    $this->db->query("TRUNCATE TABLE `".DB_PREFIX ."product_advertise_google_status` ") ;
        
        // Truncate Table product_advertise_google_target
	    $this->db->query("TRUNCATE TABLE `".DB_PREFIX ."product_advertise_google_target` ") ;
        
        // Truncate Table product_attribute
	    $this->db->query("TRUNCATE TABLE `".DB_PREFIX ."product_attribute` ") ;
        
        // Truncate Table product_description
	    $this->db->query("TRUNCATE TABLE `".DB_PREFIX ."product_description` ") ;
        
        // Truncate Table product_discount
	    $this->db->query("TRUNCATE TABLE `".DB_PREFIX ."product_discount` ") ;
        
        // Truncate Table product_filter
	    $this->db->query("TRUNCATE TABLE `".DB_PREFIX ."product_filter` ") ;
        
        // Truncate Table product_image
	    $this->db->query("TRUNCATE TABLE `".DB_PREFIX ."product_image` ") ;
        
        // Truncate Table product_option
	    $this->db->query("TRUNCATE TABLE `".DB_PREFIX ."product_option` ") ;
        
        // Truncate Table product_option_value
	    $this->db->query("TRUNCATE TABLE `".DB_PREFIX ."product_option_value` ") ;
        
        // Truncate Table product_recurring
	    $this->db->query("TRUNCATE TABLE `".DB_PREFIX ."product_recurring` ") ;
        
        // Truncate Table product_related
	    $this->db->query("TRUNCATE TABLE `".DB_PREFIX ."product_related` ") ;
        
        // Truncate Table product_reward
	    $this->db->query("TRUNCATE TABLE `".DB_PREFIX ."product_reward` ") ;
        
        // Truncate Table product_special
	    $this->db->query("TRUNCATE TABLE `".DB_PREFIX ."product_special` ") ;
        
        // Truncate Table product_to_category
	    $this->db->query("TRUNCATE TABLE `".DB_PREFIX ."product_to_category` ") ;
        
        // Truncate Table product_to_download
	    $this->db->query("TRUNCATE TABLE `".DB_PREFIX ."product_to_download` ") ;
        
        // Truncate Table product_to_layout
	    $this->db->query("TRUNCATE TABLE `".DB_PREFIX ."product_to_layout` ") ;
        
        // Truncate Table product_to_store
	    $this->db->query("TRUNCATE TABLE `".DB_PREFIX ."product_to_store` ") ;
        
        // Truncate Table oc_tax_class
	    $this->db->query("TRUNCATE TABLE `".DB_PREFIX ."tax_class` ") ;
        
        // Truncate Table oc_tax_rate
	    $this->db->query("TRUNCATE TABLE `".DB_PREFIX ."tax_rate` ") ;
        
        // Truncate Table tax_rate_to_customer_group
	    $this->db->query("TRUNCATE TABLE `".DB_PREFIX ."tax_rate_to_customer_group` ") ;
        
        // Truncate Table tax_rule
	    $this->db->query("TRUNCATE TABLE `".DB_PREFIX ."tax_rule` ") ;
        
        // Truncate Table option
	    $this->db->query("TRUNCATE TABLE `".DB_PREFIX ."option` ") ;
        
        // Truncate Table option_description
	    $this->db->query("TRUNCATE TABLE `".DB_PREFIX ."option_description` ") ;
        
        // Truncate Table option_value
	    $this->db->query("TRUNCATE TABLE `".DB_PREFIX ."option_value` ") ;
        
        // Truncate Table option_value_description
	    $this->db->query("TRUNCATE TABLE `".DB_PREFIX ."option_value_description` ") ;
        
        // Truncate Table option_value
	    $this->db->query("TRUNCATE TABLE `".DB_PREFIX ."manufacturer` ") ;
        
        // Truncate Table manufacturer_to_store
	    $this->db->query("TRUNCATE TABLE `".DB_PREFIX ."manufacturer_to_store` ") ;
        
     
	}
    
    public function AddIdColumn (){
        // Add smartlife_id Column to Table  tax_rate
	    $this->db->query(" ALTER TABLE `".DB_PREFIX ."tax_rate` ADD smartlife_id INTEGER(20)") ;
    }
    
    public function dropIdColumns(){
        // Drop smartlife_id Column From Table  tax_rate
	    $this->db->query(" ALTER TABLE `".DB_PREFIX ."tax_rate` DROP COLUMN 'smartlife_id' ") ;
    }
 
}  