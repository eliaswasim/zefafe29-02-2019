<?php

/**
* @version [Supported opencart version 3.x.x.x.]
* @category SmartLife
* @package Opencart Smart Life ERP System Connector
* @author  Muhammed Abdul Wahab Shawa | Smart Life 
* @copyright Copyright (c) 2010-2019 Smart Life Software Private Limited 
*/


class ModelSmartlifecustomer extends model{
   
   	public function getOcCustomer($selected){
	  if(!empty($selected)){
	    $sync_customerIds = implode($selected, ",");
	    $sub_query = "AND c.customer_id IN (".$sync_customerIds.")";
	  }else{
	    $sub_query = '';
	  }

	  if($this->config->get('config_store_id')){
	    $store_id = $this->config->get('config_store_id');
	  }else{
	    $store_id = 0;
	  }

	  $query =  "SELECT *,c.customer_id,c.firstname,c.lastname, z.name AS state FROM ".DB_PREFIX."customer c LEFT JOIN ".DB_PREFIX."customer_group_description cgd ON(c.customer_group_id = cgd.customer_group_id) LEFT JOIN ".DB_PREFIX."address ad ON (c.customer_id = ad.customer_id AND c.address_id = ad.address_id) LEFT JOIN ".DB_PREFIX."zone z ON ((ad.country_id = z.country_id) AND (ad.zone_id = z.zone_id)) WHERE cgd.language_id = '" . (int)$this->config->get('config_language_id') . "' AND c.store_id = '".(int)$store_id."' ".$sub_query;

	  $query =  $this->db->query($query)->rows;

	  if($query){
	    foreach ($query as $key => $customer) {
	      $sql = $this->db->query("SELECT * FROM " . DB_PREFIX . "country WHERE country_id = '".(int)$customer['country_id']."' ")->row;
	      if (isset($sql['name'])) {
	        $query[$key]['country_name'] = $sql['name'];
	      } else {
	        $query[$key]['country_name'] = '';
	      }
	    }
	  }
	  return $query;
	}
} 
