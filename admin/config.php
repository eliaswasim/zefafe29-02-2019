<?php
// HTTP
define('HTTP_SERVER', 'https://zefafe.cc/admin/');
define('HTTP_CATALOG', 'https://zefafe.cc/');

// HTTPS
define('HTTPS_SERVER', 'https://zefafe.cc/admin/');
define('HTTPS_CATALOG', 'https://zefafe.cc/');

// DIR
define('DIR_APPLICATION', '/home/zefafe/public_html/admin/');
define('DIR_SYSTEM', '/home/zefafe/public_html/system/');
define('DIR_IMAGE', '/home/zefafe/public_html/image/');
define('DIR_STORAGE', '/home/zefafe/storage-sm/');
define('DIR_CATALOG', '/home/zefafe/public_html/catalog/');
define('DIR_LANGUAGE', DIR_APPLICATION . 'language/');
define('DIR_TEMPLATE', DIR_APPLICATION . 'view/template/');
define('DIR_CONFIG', DIR_SYSTEM . 'config/');
define('DIR_CACHE', DIR_STORAGE . 'cache/');
define('DIR_DOWNLOAD', DIR_STORAGE . 'download/');
define('DIR_LOGS', DIR_STORAGE . 'logs/');
define('DIR_MODIFICATION', DIR_STORAGE . 'modification/');
define('DIR_SESSION', DIR_STORAGE . 'session/');
define('DIR_UPLOAD', DIR_STORAGE . 'upload/');

// DB
define('DB_DRIVER', 'mpdo');
define('DB_HOSTNAME', 'localhost');
define('DB_USERNAME', 'zefafe_user');
define('DB_PASSWORD', 'tqd?uqld4DC7');
define('DB_DATABASE', 'zefafe_db');
define('DB_PORT', '3306');
define('DB_PREFIX', 'oc_');

// OpenCart API
define('OPENCART_SERVER', 'https://www.opencart.com/');
