<?php

/**
* @version [Supported opencart version 3.x.x.x.]
* @category SmartLife
* @package Opencart Smart Life ERP System Connector
* @author  Muhammed Abdul Wahab Shawa | Smart Life 
* @copyright Copyright (c) 2010-2019 Smart Life Software Private Limited 
*/


class ControllerExtensionModuleSmartlife extends Controller {

	public function syncronize_customer($root = false) {

		$this->load->model('account/customer');

		$this->load->language('extension/module/smartlife');

		$counter = 0;

		if($this->config->get('module_smartlife_status') && $this->config->get('module_smartlife_api_key') && $this->config->get('module_smartlife_company_code') ){

			$this->registry->set('sl',new Smartlife($this->registry));

			$results = $this->model_account_customer->getCustomerByEmail($this->request->post['email']);
             
			if(isset($results['customer_id']) && $results['customer_id']){
				$counter = $this->sl->syncCustomer(array($results));
			}
		}
	}
    
    public function check_before_delete($route = array(), $request = array() ,$order_id = null){
        
        $this->load->model('checkout/order');
        $this->load->language('extension/module/smartlife');
         
        if (isset($this->request->get['order_id'])) {
			$order_id = $this->request->get['order_id'];
		} else {
			$order_id = 0;
		}
        
        $orderComplatedStatus = $this->config->get('config_complete_status');
		$order_info = $this->model_checkout_order->getOrder($order_id);
        if ($order_info) {
			  $order_status_id = $order_info['order_status_id'];
              if(in_array($order_status_id,$orderComplatedStatus)){
                   $json['error'] = $this->language->get('error_cant_delete_complated_order'); 
                   echo json_encode($json);
                   exit();
              }
		} 
        
    }
    public function check_order_status($route = array(), $request = array() ,$order_id = null) {
        
       
         if($this->config->get('module_smartlife_status') && $this->config->get('module_smartlife_api_key') && $this->config->get('module_smartlife_company_code') ){
            
            $this->load->model('extension/module/smartlife');
            $this->load->language('extension/module/smartlife');
            $json = array();
            $orderComplatedStatus = $this->config->get('config_complete_status');
            
			if (isset($this->session->data['order_id']) && $this->session->data['order_id']) {
				$order_id = $this->session->data['order_id'];
			} elseif (isset($this->request->get['order_id']) && $this->request->get['order_id']) {
				$order_id = $this->request->get['order_id'];
			} elseif (isset($request[0]) && $request[0]) {
			 		$order_id = $request[0];
			}
            
            $order = $this->model_extension_module_smartlife->getAllOcOrder($order_id);
            
            if(is_array($order)){
                 
                foreach($order as $one){
                    $order_status_id = $one['order_status_id']; 
                    
                    if(in_array($order_status_id, $orderComplatedStatus)){
                        $json['error'] = $this->language->get('error_is_complated_order');
                        $this->response->addHeader('Content-Type: application/json');
                        echo json_encode($json) ; 
                        exit();
                    }
                }
            }
            
        }  
    }
     
	public function syncronize_order($route = array(), $request = array() ,$order_id = null) {

		$json = array();

		$this->load->model('extension/module/smartlife');

		$this->load->language('extension/module/smartlife');

		$counter = 0;

		if($this->config->get('module_smartlife_status') && $this->config->get('module_smartlife_api_key') && $this->config->get('module_smartlife_company_code') ){
            
            $orderComplatedStatus = $this->config->get('config_complete_status');
            
            if(isset($request[1]) and in_array($request[1], $orderComplatedStatus)){
    			if (isset($this->session->data['order_id']) && $this->session->data['order_id']) {
    				$order_id = $this->session->data['order_id'];
    			} elseif (isset($this->request->get['order_id']) && $this->request->get['order_id']) {
    				$order_id = $this->request->get['order_id'];
    			} elseif (isset($request[0]) && $request[0]) {
    			 		$order_id = $request[0];
    			}
                
                $customer_id = 0;
                $order = $this->model_extension_module_smartlife->getAllOcOrder($order_id);
                
    			if (isset($this->session->data['customer_id']) && $this->session->data['customer_id']) {
    				$customer_id = $this->session->data['customer_id'];
    			} elseif (isset($this->session->data['customer']['customer_id']) && $this->session->data['customer']['customer_id']) {
    				$customer_id = $this->session->data['customer']['customer_id'];
    			}elseif(isset($order[0]['customer_id']) and $order[0]['customer_id'] != 0){
    			 $customer_id = $order[0]['customer_id'];
    			}
            
            

    			if(isset($order_id) && $order_id && isset($customer_id)){
    
    				$total_array = array();
    
    				$total_string = '';
    
    				$order_totals = $this->model_extension_module_smartlife->getOrderTotals($order_id);
    
    				$shipping_total = 0;
    
    				$discount_total = 0;
    
    				$tax_total = 0;
    
    				foreach ($order_totals as $key => $totals) {
    				  $total_string = $total_string.nl2br($totals['title'].' : '.$totals['value']."\n");
    				  if ($totals['code'] == 'shipping') {
    				    $shipping_total += $totals['value'];
    				  }
    
    				  if ($totals['code'] == 'coupon' || $totals['code'] == 'voucher') {
    				    $discount_total += abs($totals['value']);
    				  }
    
    				  if ($totals['code'] == 'tax') {
    				    $tax_total += $totals['value'];
    				  }
    				}
    
    				
                    
    				if ($order) {
    					$order[0]['customer_id'] = $customer_id;
    
    					$order[0]['order_id'] = $order_id;
    
    					$order[0]['order_totals'] = $total_string;
    
    					$order[0]['shipping_total'] = $shipping_total;
    
    					$order[0]['discount_total'] = $discount_total;
    
    					$order[0]['tax_total'] = $tax_total;
    
    					$order[0]['order_products'] = $this->model_extension_module_smartlife->getOrderPrdoucts($order_id);
    
    					$this->registry->set('sl',new Smartlife($this->registry));
    
    					$counter = $this->sl->syncOrder($order);
    				}
    
    				$json['info'] = sprintf($this->language->get('text_next_success_syncronize_order'), $counter);
    
    			} 
    			 
            }
		}

		
	}
}
?>
