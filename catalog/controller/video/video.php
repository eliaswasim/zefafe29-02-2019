<?php
class ControllerVideoVideo extends Controller {
    
    public function index() {
		$this->load->language('catalog/video');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('catalog/video');

		$this->getList();
	}
    
      
	public function show() {
		$this->load->language('catalog/video');

		$this->load->model('catalog/video');

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/home')
		);

		if (isset($this->request->get['video_id'])) {
			$video_id = (int)$this->request->get['video_id'];
		} else {
			$video_id = 0;
		}

		$video_info = $this->model_catalog_video->getVideo($video_id);

		if ($video_info) {
			$this->document->setTitle($video_info['meta_title']);
			$this->document->setDescription($video_info['meta_description']);
			$this->document->setKeywords($video_info['meta_keyword']);

			$data['breadcrumbs'][] = array(
				'text' => $video_info['title'],
				'href' => $this->url->link('video/video', 'video_id=' .  $video_id)
			);

			$data['heading_title'] = $video_info['title'];
            $data['image'] = $video_info['image'];
            $data['type'] = $video_info['type'];
            $data['link'] = $video_info['link'];
            $data['youtube_id'] = $video_info['youtube_id'];
            
            
			$data['description'] = html_entity_decode($video_info['description'], ENT_QUOTES, 'UTF-8');

			$data['continue'] = $this->url->link('common/home');

			$data['column_left'] = $this->load->controller('common/column_left');
			$data['column_right'] = $this->load->controller('common/column_right');
			$data['content_top'] = $this->load->controller('common/content_top');
			$data['content_bottom'] = $this->load->controller('common/content_bottom');
			$data['footer'] = $this->load->controller('common/footer');
			$data['header'] = $this->load->controller('common/header');

			$this->response->setOutput($this->load->view('catalog/video', $data));
		} else {
			$data['breadcrumbs'][] = array(
				'text' => $this->language->get('text_error'),
				'href' => $this->url->link('video/video', 'video_id=' . $video_id)
			);

			$this->document->setTitle($this->language->get('text_error'));

			$data['heading_title'] = $this->language->get('text_error');

			$data['text_error'] = $this->language->get('text_error');

			$data['continue'] = $this->url->link('common/home');

			$this->response->addHeader($this->request->server['SERVER_PROTOCOL'] . ' 404 Not Found');

			$data['column_left'] = $this->load->controller('common/column_left');
			$data['column_right'] = $this->load->controller('common/column_right');
			$data['content_top'] = $this->load->controller('common/content_top');
			$data['content_bottom'] = $this->load->controller('common/content_bottom');
			$data['footer'] = $this->load->controller('common/footer');
			$data['header'] = $this->load->controller('common/header');

			$this->response->setOutput($this->load->view('error/not_found', $data));
		}
	}
   
	protected function getList() {
		if (isset($this->request->get['sort'])) {
			$sort = $this->request->get['sort'];
		} else {
			$sort = 'id.title';
		}

		if (isset($this->request->get['order'])) {
			$order = $this->request->get['order'];
		} else {
			$order = 'ASC';
		}

		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}

		$url = '';

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/home')
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('catalog/video',   $url, true)
		);

		$data['add'] = $this->url->link('catalog/video/add',  $url, true);
		$data['delete'] = $this->url->link('catalog/video/delete',   $url, true);

		$data['videos'] = array();

		$filter_data = array(
			'sort'  => $sort,
			'order' => $order,
			'start' => ($page - 1) * $this->config->get('config_limit_admin'),
			'limit' => $this->config->get('config_limit_admin')
		);

		$video_total = $this->model_catalog_video->getTotalVideos();

		$results = $this->model_catalog_video->getVideos($filter_data);
        $this->load->model('tool/image');
        
		foreach ($results as $result) {
		  
          if ($result['image']) {
				$thumb = $this->model_tool_image->resize($result['image'],  540, 300);
			} else {
				$thumb = '';
			}
			$data['videos'][] = array(
				'video_id' => $result['video_id'],
				'title'          => $result['title'],
                'description'    =>  mb_substr(strip_tags(html_entity_decode($result['description'])), 0 , 250,'UTF-8').'...' ,
                'url'=>$this->url->link('video/video/show', 'video_id=' . $result['video_id']),
                'image'          => $thumb,
                'type'          => $result['type'],
                'youtube_id'          => $result['youtube_id'],
                'link'          => $result['link'],
				'sort_order'     => $result['sort_order'],
				'edit'           => $this->url->link('catalog/video/edit',    '&video_id=' . $result['video_id'] . $url, true)
			);
		}

		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		if (isset($this->session->data['success'])) {
			$data['success'] = $this->session->data['success'];

			unset($this->session->data['success']);
		} else {
			$data['success'] = '';
		}

		if (isset($this->request->post['selected'])) {
			$data['selected'] = (array)$this->request->post['selected'];
		} else {
			$data['selected'] = array();
		}

		$url = '';

		if ($order == 'ASC') {
			$url .= '&order=DESC';
		} else {
			$url .= '&order=ASC';
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$data['sort_title'] = $this->url->link('catalog/video',  '&sort=id.title' . $url, true);
		$data['sort_sort_order'] = $this->url->link('catalog/video', '&sort=i.sort_order' . $url, true);

		$url = '';

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		$pagination = new Pagination();
		$pagination->total = $video_total;
		$pagination->page = $page;
		$pagination->limit = $this->config->get('config_limit_admin');
		$pagination->url = $this->url->link('catalog/video',     $url . '&page={page}', true);

		$data['pagination'] = $pagination->render();

		$data['results'] = sprintf($this->language->get('text_pagination'), ($video_total) ? (($page - 1) * $this->config->get('config_limit_admin')) + 1 : 0, ((($page - 1) * $this->config->get('config_limit_admin')) > ($video_total - $this->config->get('config_limit_admin'))) ? $video_total : ((($page - 1) * $this->config->get('config_limit_admin')) + $this->config->get('config_limit_admin')), $video_total, ceil($video_total / $this->config->get('config_limit_admin')));

		$data['sort'] = $sort;
		$data['order'] = $order;

		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('catalog/video_list', $data));
	}
}