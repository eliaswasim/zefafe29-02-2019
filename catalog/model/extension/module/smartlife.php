<?php

/**
* @version [Supported opencart version 3.x.x.x.]
* @category SmartLife
* @package Opencart Smart Life ERP System Connector
* @author  Muhammed Abdul Wahab Shawa | Smart Life 
* @copyright Copyright (c) 2010-2019 Smart Life Software Private Limited 
*/

class ModelExtensionModuleSmartlife extends Model {

	/**
	 * [getOrderTotals is used to get the order totals]
	 * @param  [integer] $order_id [order id]
	 * @return [array]           [order totals]
	 */
	public function getOrderTotals($order_id = null){
		if($order_id){
			$query = $this->db->query("SELECT ot.title, ot.value, ot.code FROM `" . DB_PREFIX . "order` o LEFT JOIN ".DB_PREFIX."order_total ot ON (o.order_id = ot.order_id) WHERE ot.order_id = '".(int)$order_id."' ")->rows;
		}else{
			$query = array();
		}

		return $query;
	}

	/**
	 * [getOrderPrdoucts is used to get the products for a particular order]
	 * @param  [integer] $order_id [order_id]
	 * @return [array]           [order product details]
	 */
	public function getOrderPrdoucts($order_id = null){
		if($order_id){
			$query = $this->db->query("SELECT op.* FROM ".DB_PREFIX."order_product op LEFT JOIN `" . DB_PREFIX . "order` o ON (op.order_id = o.order_id) LEFT JOIN ".DB_PREFIX."product p ON (op.product_id = p.product_id) WHERE op.order_id = '".(int)$order_id."' ")->rows;
		}else{
			$query = array();
		}
         
        foreach ($query as $key=> $product){
            $products[$key] = $product;
            $order_product_id = $product['order_product_id'];
             
            $query = $this->db->query("SELECT  * FROM ".DB_PREFIX."order_option        WHERE order_id = '".(int)$order_id."' AND order_product_id = '".(int) $order_product_id."'")->row;
           
            $products[$key]['option_id'] = isset($query['product_option_id']) ? $query['product_option_id'] : null;
        }
        
		return $products;
	}

	/**
	 * [getAllOcOrder is used to get the order details if not synchronized]
	 * @param  integer $order_id [order id]
	 * @return [array]            [order details]
	 */
	public function getAllOcOrder($order_id = 0){

		if($order_id){
			$sub_query = "AND o.order_id = '".$order_id."'";
		}else{
			$sub_query = '';
		}

		if($this->config->get('config_store_id')){
			$store_id = $this->config->get('config_store_id');
		}else{
			$store_id = 0;
		}


		if (isset($order_id) AND $order_id) {
			$query = "SELECT DISTINCT o.*, os.name AS order_status FROM `" . DB_PREFIX . "order` o LEFT JOIN ".DB_PREFIX."customer cust ON (o.customer_id = cust.customer_id) LEFT JOIN ".DB_PREFIX."order_status os ON (o.order_status_id = os.order_status_id) WHERE o.language_id = '".(int)$this->config->get('config_language_id')."' AND o.store_id = '".(int)$store_id."' ".$sub_query;
		} else{
			$query = "SELECT DISTINCT o.*, os.name AS order_status FROM `" . DB_PREFIX . "order` o LEFT JOIN ".DB_PREFIX."customer cust ON (o.customer_id = cust.customer_id) LEFT JOIN ".DB_PREFIX."order_status os ON (o.order_status_id = os.order_status_id) WHERE o.language_id = '".(int)$this->config->get('config_language_id')."' AND os.language_id = '".(int)$this->config->get('config_language_id')."' AND o.store_id = '".(int)$store_id."' ".$sub_query;
		}

		$query =  $this->db->query($query)->rows;

		return $query;
	}

}

 ?>
