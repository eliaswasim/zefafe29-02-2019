<?php
//  Website: WWW.OpenCartArab.com
//  E-Mail : info@OpenCartArab.com

// Heading
$_['heading_title']  = 'تغيير كلمة المرور';

// Text
$_['text_account']   = 'الحساب';
$_['text_password']  = 'كلمة المرور الجديدة';
$_['text_success']   = 'كلمة المرور الخاصة بك قد تم تحديثها بنجاح.';

// Entry
$_['entry_password'] = 'كلمة المرور الجديدة';
$_['entry_confirm']  = 'تأكيد كلمة المرور الجديدة';

// Error
$_['error_password'] = 'كلمة المرور يجب أن تكون أكثر من 3 وأقل من 20 حرفا!';
$_['error_confirm']  = 'لم تتطابق كلمة المرور مع تأكيد كلمة المرور الرجاء إعادة المحاولة مرة أخرى!';


// list Titles
$_['list_my_account']       = 'حسابي';
$_['list_account']       = 'تحرير الحساب';
$_['list_pass']       = 'كلمة المرور';
$_['list_wish']       = 'قائمة الأمنيات';
$_['list_orders']       = 'طلباتي';
$_['list_return']       = 'المنتجات المرتجعة';
$_['list_address']       = 'دفتر العناوين';
$_['list_logout']       = 'تسجيل الخروج';
 