<?php
//  Website: WWW.OpenCartArab.com
//  E-Mail : info@OpenCartArab.com

// Heading
$_['heading_title']    = 'الاشتراك في القائمة البريدية';

// Text
$_['text_account']     = 'الحساب';
$_['text_newsletter']  = 'القائمة البريدية';
$_['text_success']     = 'تم التعديل !';
$_['button_continue45']  = 'حفظ';
// Entry
$_['entry_newsletter'] = 'اشترك:';

// list Titles
$_['list_my_account']       = 'حسابي';
$_['list_account']       = 'تحرير الحساب';
$_['list_pass']       = 'كلمة المرور';
$_['list_wish']       = 'قائمة الأمنيات';
$_['list_orders']       = 'طلباتي';
$_['list_return']       = 'المنتجات المرتجعة';
$_['list_address']       = 'دفتر العناوين';
$_['list_logout']       = 'تسجيل الخروج';
$_['button_continue22']       = 'متابعة';