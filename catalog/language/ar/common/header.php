<?php
//  Website: WWW.OpenCartArab.com
//  E-Mail : info@OpenCartArab.com

// Text
$_['text_home']          = 'الرئيسية';
$_['text_wishlist']      = 'قائمة الأمنيات (%s)';
$_['text_shopping_cart'] = 'سلة الشراء';
$_['text_category']      = 'أقسام الموقع';
$_['text_account']       = 'حسابي';
$_['text_register']      = 'تسجيل جديد';
$_['text_login']         = 'تسجيل الدخول';
$_['text_order']         = 'طلباتي';
$_['text_transaction']   = 'رصيدي';
$_['text_download']      = 'ملفات التنزيل';
$_['text_logout']        = 'تسجيل الخروج';
$_['text_checkout']      = 'إنهاء الطلب';
$_['text_search']        = 'بحث';
$_['text_all']           = 'اذهب الى قسم';


$_['text_1']           = 'الدفع نقدا عند الاستلام';
$_['text_2']           = 'الشحن داخل السعودية';
$_['text_3']           = 'دعم خلال أوقات العمل';

$_['text_login_1']           = 'تسجيل الدخول';
$_['text_register_1']           = 'تسجيل حساب جديد'; 
$_['text_new_account_1']           = 'يمكنك تسجيل حساب جديد';
$_['text_here']           = 'من هنا';
$_['text_search_words']           = 'كلمات البحث';
 

$_['text_profile']           = 'الملف الشخصي';
$_['text_my_orders']           = 'طلباتي';
 $_['text_help']           = 'المساعدة';

$_['hello'] = 'مرحبا بك في زفافي';
 
 

