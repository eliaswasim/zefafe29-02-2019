<?php
//  Website: WWW.OpenCartArab.com
//  E-Mail : info@OpenCartArab.com

// Heading
$_['heading_title']  = 'اتصل بنا';

// Text 
$_['text_location']  = 'موقعنا';
$_['text_store']     = 'متاجرنا';
$_['text_contact']   = 'نموذج الاتصال';
$_['text_address']   = 'العنوان';
$_['text_telephone'] = 'رقم الهاتف أو الجوال';
$_['text_fax']       = 'فاكس';
$_['text_open']      = 'أوقات العمل';
$_['text_comment']   = 'ملاحظات';
$_['text_success']   = '<p>تم الارسال بنجاح لفريق عمل الموقع !</p>';

$_['text_message']   = '<p>تم الارسال بنجاح لفريق عمل الموقع !</p>';


// Entry
$_['entry_name']     = 'الاسم';
$_['entry_email']    = 'البريد الالكتروني';
$_['entry_enquiry']  = 'الموضوع';

// Email
$_['email_subject']  = 'مراسلة المتجر %s';

// Errors
$_['error_name']     = 'يجب أن يكون الاسم أكثر من 3 وأقل من 32 حرفا!';
$_['error_email']    = 'البريد الإلكتروني خطأ, الرجاء إعادة ادخال البريد الاكتروني بشكل صحيح !';
$_['error_enquiry']  = 'الاستفسار يجب أن يكون أكثر من 10 وأقل من 3000 حرف!';
$_['text_contact1'] = '  هناك حقيقة مثبتة منذ زمن طويل وهي أن المحتوى المقروء لصفحة ما سيلهي القارئ عن التركيز على الشكل الخارجي للنص أو شكل توضع الفقرات في الصفحة التي ';
$_['text_contact2'] = '  هناك حقيقة مثبتة منذ زمن طويل وهي أن المحتوى المقروء لصفحة ما سيلهي القارئ عن التركيز على الشكل الخارجي للنص أو شكل توضع الفقرات في الصفحة التي ';
$_['text_contact3'] = ' متجر بيوتي سلكت';
$_['text_contact4'] = '  معلومات التواصل';
 

