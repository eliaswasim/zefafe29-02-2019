<?php
// Text
$_['text_home']          = 'Home';
$_['text_wishlist']      = 'Wish List (%s)';
$_['text_shopping_cart'] = 'Shopping Cart';
$_['text_category']      = 'Categories';
$_['text_account']       = 'My Account';
$_['text_register']      = 'Register';
$_['text_login']         = 'Login';
$_['text_order']         = 'Order History';
$_['text_transaction']   = 'Transactions';
$_['text_download']      = 'Downloads';
$_['text_logout']        = 'Logout';
$_['text_checkout']      = 'Checkout';
$_['text_search']        = 'Search';
$_['text_all']           = 'Show All';
$_['text_login_1']           = 'Login';
$_['text_register_1']           = 'Register';
$_['text_new_account_1']           = 'You can register a new account';
$_['text_here']           = 'from here';
$_['text_search_words']           = 'Search here';

$_['text_profile']           = 'My Profile';
$_['text_my_orders']           = 'My Orders';
$_['text_help']           = 'Support';