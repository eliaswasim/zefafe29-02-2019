<?php
// Heading
$_['heading_title']    = 'Newsletter Subscription';

// Text
$_['text_account']     = 'Account';
$_['text_newsletter']  = 'Newsletter';
$_['button_continue45']  = 'Save';

$_['text_success']     = 'Success: Your newsletter subscription has been successfully updated!';

// Entry
$_['entry_newsletter'] = 'Subscribe';
$_['list_my_account']       = 'My Account';
$_['list_account']       = 'Edit Account';
$_['list_pass']       = 'Edit Password';
$_['list_wish']       = 'Wish list';
$_['list_orders']       = 'My Orders';
$_['list_return']       = 'Returned Products';
$_['list_address']       = 'Address Book';
$_['list_logout']       = 'Sign out';