<?php
// Heading
$_['heading_title']       = 'My Account';

// Text
$_['text_account']        = 'Account';
$_['text_my_account']     = 'My Account';
$_['text_my_orders']      = 'My Orders';
$_['text_my_affiliate']   = 'My Affiliate Account';
$_['text_my_newsletter']  = 'Newsletter';
$_['text_edit']           = 'Edit account';
$_['text_password']       = 'Change your password';
$_['text_address']        = 'Modify   address book ';
$_['text_credit_card']    = 'Manage Stored Credit Cards';
$_['text_wishlist']       = 'Modify your wish list';
$_['text_order']          = 'View your order history';
$_['text_download']       = 'Downloads';
$_['text_reward']         = 'Your Reward Points';
$_['text_return']         = 'View your return requests';
$_['text_transaction']    = 'Your Transactions';
$_['text_newsletter']     = 'Subscribe / unsubscribe to newsletter';
$_['text_recurring']      = 'Recurring payments';
$_['text_transactions']   = 'Transactions';
$_['text_affiliate_add']  = 'Register for an affiliate account';
$_['text_affiliate_edit'] = 'Edit your affiliate information';
$_['text_tracking']       = 'Custom Affiliate Tracking Code';


// list Titles

$_['list_account']       = 'Edit Account';
$_['list_pass']       = 'Edit Password';
$_['list_wish']       = 'Wish list';
$_['list_orders']       = 'My Orders';
$_['list_return']       = 'Returned Products';
$_['list_address']       = 'Address Book';
$_['list_logout']       = 'Sign out';
 