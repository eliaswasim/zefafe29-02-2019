<?php
// Heading
$_['heading_title'] = 'My Wish List';

// Text
$_['text_account']  = 'Account';
$_['text_instock']  = 'In Stock';
$_['text_wishlist'] = 'Wish List (%s)';
$_['text_login']    = 'You must <a href="%s">login</a> or <a href="%s">create an account</a> to save <a href="%s">%s</a> to your <a href="%s">wish list</a>!';
$_['text_success']  = 'Success: You have added <a href="%s">%s</a> to your <a href="%s">wish list</a>!';
$_['text_remove']   = 'Success: You have modified your wish list!';
$_['text_empty']    = 'Your wish list is empty.';
$_['text_empty2']    = 'Your wish list is empty.';
// Column
$_['column_image']  = 'Image';
$_['column_name']   = 'Product Name';
$_['column_model']  = 'Model';
$_['column_stock']  = 'Stock';
$_['column_price']  = 'Unit Price';
$_['column_action'] = 'Action';


$_['list_my_account']       = 'My Account';
$_['list_account']       = 'Edit Account';
$_['list_pass']       = 'Edit Password';
$_['list_wish']       = 'Wish list';
$_['list_orders']       = 'My Orders';
$_['list_return']       = 'Returned Products';
$_['list_address']       = 'Address Book';
$_['list_logout']       = 'Sign out';