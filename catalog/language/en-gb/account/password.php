<?php
// Heading
$_['heading_title']  = 'Change Password';

// Text
$_['text_account']   = 'Account';
$_['text_password']  = 'Your Password';
$_['text_success']   = 'Success: Your password has been successfully updated.';

// Entry
$_['entry_password'] = 'Password';
$_['entry_confirm']  = 'Password Confirm';

// Error
$_['error_password'] = 'Password must be between 4 and 20 characters!';
$_['error_confirm']  = 'Password confirmation does not match password!';

$_['list_my_account']       = 'My Account';
$_['list_account']       = 'Edit Account';
$_['list_pass']       = 'Edit Password';
$_['list_wish']       = 'Wish list';
$_['list_orders']       = 'My Orders';
$_['list_return']       = 'Returned Products';
$_['list_address']       = 'Address Book';
$_['list_logout']       = 'Sign out';