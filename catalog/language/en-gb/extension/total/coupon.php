<?php
// Heading
$_['heading_title'] = 'Use Coupon Code';
$_['heading_title2'] = 'You can add code for discount coupons';


// Text
$_['text_coupon']   = 'Coupon (%s)';
$_['text_success']  = 'Success: Your coupon discount has been applied!';

// Entry
$_['entry_coupon']  = 'Enter your coupon here';

// Error
$_['error_coupon']  = 'Warning: Coupon is either invalid, expired or reached its usage limit!';
$_['error_empty']   = 'Warning: Please enter a coupon code!';
$_['apply_text'] = 'Apply';