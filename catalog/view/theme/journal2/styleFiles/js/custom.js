$(document).ready(function(){

    // Back to top button
    $(window).scroll(function() {
        if ($(this).scrollTop() > 100) {
          jQuery('.back-to-top').fadeIn('slow');
        } else {
          jQuery('.back-to-top').fadeOut('slow');
        }
    });

    $('.back-to-top').click(function() {
        jQuery('html, body').animate({
          scrollTop: 0
        }, 1500, 'easeInOutExpo');
        return false;
    });


    // product grid / list veiw
    $('#gridviewbtn').on('click', function () {
        $('.grid-view').removeClass('hidden');
        $('.list-view').addClass('hidden');
        $(this).addClass('active');
        $('#listviewbtn').removeClass('active');
    });
    $('#listviewbtn').on('click', function () {
        $('.list-view').removeClass('hidden');
        $('.grid-view').addClass('hidden');
        $(this).addClass('active');
        $('#gridviewbtn').removeClass('active');
    });


    // price filter
    $( "#slider-range" ).slider({
      range: true,
      min: 0,
      max: 5000,
      values: [ 0, 5000 ],
      slide: function( event, ui ) {
        $( ".rangeValues" ).text( "$" + ui.values[ 0 ] + " - $" + ui.values[ 1 ] );
      }
    });
    $( ".rangeValues" ).text( "$" + $( "#slider-range" ).slider( "values", 0 ) +
      " - $" + $( "#slider-range" ).slider( "values", 1 ) );


    $('.open-filter').on('click', function () {
        $('.filter-side-bar').toggle();
    });

    // Sucess alert
    $('#prodAdd').on('click', function () {
        $('.error-alert').fadeIn('slow');
        setTimeout(function() {
          $('.error-alert').fadeOut('slow');
        }, 5000);
    });

    $('.close-alert').on('click', function () {
        $('.alert').fadeOut('slow');
    });

    $('.add-pro-to-fav').on('click', function () {
        $('.sucess-alert').fadeIn('slow');
        setTimeout(function() {
          $('.sucess-alert').fadeOut('slow');
        }, 5000);
    });


    // add to cart button click
    $('.product-item .white-btn').on('click', function () {
        $(this).addClass('disabled');
        $(this).text('تمت الإضافة');
    });

    // menu login /register
    // $('.show-login').on('click', function () {
    //     $('.menu').addClass('hidden');
    //     $('.login-div').removeClass('hidden');
    //     $('.register-div').addClass('hidden');
    // });

    // $('.show-register').on('click', function () {
    //     $('.login-div').addClass('hidden');
    //     $('.register-div').removeClass('hidden');
    // });

    // $('.forget-pass').on('click', function () {
    //     $('.login-div').addClass('hidden');
    //     $('.menu-change-pass').removeClass('hidden');
    // });
    // $('.close-button').on('click', function () {
    //     $('.login-div').addClass('hidden');
    //     $('.menu-change-pass').addClass('hidden');
    //     $('.register-div').addClass('hidden');
    //     $('.menu').removeClass('hidden');
    // });

    // preview img of product
    $('.pro-img').on('click', function () {
        $('.pro-img').removeClass('slected-img');
        $(this).addClass('slected-img');
    });

    // image zoom
    $('.jqzoom').jqzoom({
        zoomType: 'innerzoom',
        preloadImages: false,
        alwaysOn: false,
        title: false
    });
    
    $('.product-photo .far.fa-heart').removeClass('hidden');
    $('.product-photo .fas.fa-heart').addClass('hidden');
    
    $('.product-photo .far.fa-heart').on('click', function () {
        $(this).addClass('hidden');
        $(this).parent('button').parent('.product-photo').find('.fas.fa-heart').removeClass('hidden');
    });
    
    $('.product-photo .fas.fa-heart').on('click', function () {
        $(this).addClass('hidden');
        $(this).parent('button').parent('.product-photo').find('.far.fa-heart').removeClass('hidden');
    });
    
    $('.add-pro-to-fav .fas.fa-heart').on('click', function () {
        $(this).addClass('hidden');
        $(this).parent('button').find('.far.fa-heart').removeClass('hidden');
    });
    $('.add-pro-to-fav .far.fa-heart').on('click', function () {
        $(this).addClass('hidden');
        $(this).parent('button').find('.fas.fa-heart').removeClass('hidden');
    });
    
});

// rating
$(function (){
    $('.rating').rating({
        rtl: true
    });
});



// plus /minus item quantity
function wcqib_refresh_quantity_increments() {
    jQuery("div.quantity:not(.buttons_added), td.quantity:not(.buttons_added)").each(function(a, b) {
        var c = jQuery(b);
        c.addClass("buttons_added"), c.children().first().before('<input type="button" value="-" class="minus" />'), c.children().last().after('<input type="button" value="+" class="plus" />')
    })
}
String.prototype.getDecimals || (String.prototype.getDecimals = function() {
    var a = this,
        b = ("" + a).match(/(?:\.(\d+))?(?:[eE]([+-]?\d+))?$/);
    return b ? Math.max(0, (b[1] ? b[1].length : 0) - (b[2] ? +b[2] : 0)) : 0
}), jQuery(document).ready(function() {
    wcqib_refresh_quantity_increments()
}), jQuery(document).on("updated_wc_div", function() {
    wcqib_refresh_quantity_increments()
}), jQuery(document).on("click", ".plus, .minus", function() {
    var a = jQuery(this).closest(".quantity").find(".qty"),
        b = parseFloat(a.val()),
        c = parseFloat(a.attr("max")),
        d = parseFloat(a.attr("min")),
        e = a.attr("step");
    b && "" !== b && "NaN" !== b || (b = 0), "" !== c && "NaN" !== c || (c = ""), "" !== d && "NaN" !== d || (d = 0), "any" !== e && "" !== e && void 0 !== e && "NaN" !== parseFloat(e) || (e = 1), jQuery(this).is(".plus") ? c && b >= c ? a.val(c) : a.val((b + parseFloat(e)).toFixed(e.getDecimals())) : d && b <= d ? a.val(d) : b > 0 && a.val((b - parseFloat(e)).toFixed(e.getDecimals())), a.trigger("change")
});


// filter price range 
// function getVals(){
//     // Get slider values
//     var parent = this.parentNode;
//     var slides = parent.getElementsByTagName("input");
//     var slide1 = parseFloat( slides[0].value );
//     var slide2 = parseFloat( slides[1].value );
//     // Neither slider will clip the other, so make sure we determine which is larger
//     if( slide1 > slide2 ){ var tmp = slide2; slide2 = slide1; slide1 = tmp; }

//     var displayElement = parent.getElementsByClassName("rangeValues")[0];
//       displayElement.innerHTML = "SAR " + slide1 + " - " + slide2 + " SAR";
// }
// window.onload = function(){
//     // Initialize Sliders
//     var sliderSections = document.getElementsByClassName("range-slider");
//       for( var x = 0; x < sliderSections.length; x++ ){
//         var sliders = sliderSections[x].getElementsByTagName("input");
//         for( var y = 0; y < sliders.length; y++ ){
//           if( sliders[y].type ==="range" ){
//             sliders[y].oninput = getVals;
//             // Manually trigger event first time to display values
//             sliders[y].oninput();
//         }
//     }
//   }
// }

