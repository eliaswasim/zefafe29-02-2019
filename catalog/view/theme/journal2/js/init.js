 
  
$(window).load(function() {
 
    $('#search input[name=\'search\']').parent().find('button').off('click').on('click', function (e) {
        e.preventDefault();
        e.stopPropagation();

        var url = $('base').attr('href') + 'index.php?route=product/search';

        var value = $('header #search input[name=\'search\']').val();

        if (value) {
            url += '&search=' + encodeURIComponent(value);
        }

        if (Journal.searchInDescription) {
            url += '&description=true';
        }

        location = url;
    });
});

 

// quick checkout trigger loading off
if ($('html').hasClass('quick-checkout-page')) {
	$(document).ajaxComplete(function (event, xhr, settings) {
		if (settings.url.indexOf('payment/') !== -1) {
			var json = xhr.responseJSON;

			try {
				if (json.error) {
					triggerLoadingOff();
				}
			} catch (e) {
				console.warn(e);
			}
		}
	});
}

// gdpr
if (Journal.contactPrivacy) {
	$(function () {
		$('.route-information-contact form input[type="submit"]').on('click', function () {
			if ($(this).parent().find('agree') && !$(this).parent().find('input[name="agree"]').is(':checked')) {
				alert(Journal.contactPrivacyError);
				return false;
			}
		});

		$.colorbox && $('.colorbox').colorbox({
			width: 640,
			height: 480
		});
	});
}
