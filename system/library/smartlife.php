<?php

/**
* @version [Supported opencart version 3.x.x.x.]
* @category SmartLife
* @package Opencart Smart Life ERP System Connector
* @author  Muhammed Abdul Wahab Shawa | Smart Life
* @copyright Copyright (c) 2010-2019 Smart Life Software Private Limited
*/

class Smartlife {


    private $url = 'https://smarterp.top/api/API/';
	private $registry;
    private $api_key;
    private $api_company;
    private $image_dir = 'smartlife/' ;

    public function __construct($registry) {
		$this->registry = $registry;
		$this->api_key = $this->config->get('module_smartlife_api_key');
        $this->api_company = $this->config->get('module_smartlife_company_code');

	}

    public function __get($name) {
		return $this->registry->get($name);
	}

    public function compareDates($date_one , $date_two){
        $d1 = new DateTime($date_one);
        $d2 = new DateTime($date_two);
        return true;
        return ($d1 > $d2);
    }



    public function call($uri, $args = array(), $request_type = "GET") {

        $get_params = array(
            'api_key' => $this->api_key,
            'company' => $this->api_company
        );

        if($request_type == 'GET'){
             $get_params = array_merge($args , $get_params);
        }
        $params =  http_build_query($get_params, '', "&");


        if($request_type == 'GET'){
            $fields = array();
        }else{
            $fields =  http_build_query($args, '', "&");

        }

        if($request_type == 'GET'){
            $url = $this->url . $uri.'?'.$params;
        }else{
            $url = $this->url . $uri.'?'.$params;
        }


        $headers = array();

		$defaults = array(
            CURLOPT_HEADER      	=> 0,
            CURLOPT_HTTPHEADER      => $headers,
			CURLOPT_POST            => false,
			CURLOPT_URL             => $url ,
			CURLOPT_USERAGENT       => 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.1) Gecko/20061204 Firefox/2.0.0.1',
			CURLOPT_FRESH_CONNECT   => 1,
			CURLOPT_RETURNTRANSFER  => 1,
			CURLOPT_FORBID_REUSE    => 1,
			CURLOPT_TIMEOUT         => 30,
			CURLOPT_SSL_VERIFYPEER  => 0,
			CURLOPT_SSL_VERIFYHOST  => 0,
			//CURLOPT_POSTFIELDS      => $fields,
            CURLOPT_PROTOCOLS       => CURLPROTO_HTTPS,
            CURLOPT_CUSTOMREQUEST   => $request_type
		);


        if($request_type == 'POST'){
            $defaults[CURLOPT_POSTFIELDS]  = $fields ;
        }
		$curl = curl_init();

		curl_setopt_array($curl, $defaults);

		$response = curl_exec($curl);


        $httpcode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
        curl_close($curl);

        if($httpcode != 200){
            return false;
        }


        $response = json_decode($response,true);

        if(!$response['status'] or !isset($response['status'])){
            return false;
        }

		return $response;
	}


    private function getImage($url) {

        $headers[] = 'Accept: image/gif, image/x-bitmap, image/jpeg, image/pjpeg';
        $headers[] = 'Connection: Keep-Alive';
        $headers[] = 'Content-type: application/x-www-form-urlencoded;charset=UTF-8';
        $user_agent = 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.1) Gecko/20061204 Firefox/2.0.0.1';
        $process = curl_init($url);
        curl_setopt($process, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($process, CURLOPT_HEADER, 0);
        curl_setopt($process, CURLOPT_USERAGENT, $user_agent); //check here
        curl_setopt($process, CURLOPT_TIMEOUT, 30);
        curl_setopt($process, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($process, CURLOPT_FOLLOWLOCATION, 1);
        $return = curl_exec($process);
        curl_close($process);
        return $return;

    }
    private function is_url_exist($url){
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_NOBODY, true);
        curl_exec($ch);
        $code = curl_getinfo($ch, CURLINFO_HTTP_CODE);

        if($code == 200){
           $status = true;
        }else{
          $status = false;
        }
        curl_close($ch);
       return $status;
    }

    public function saveImage($url,$product_id,$folder = ''){

        if($folder != ''){
            $dir     = DIR_IMAGE.$this->image_dir.$folder.'/'.$product_id;
            $dir_url = $this->image_dir.$folder.'/'.$product_id;
        }else{
            $dir     = DIR_IMAGE.$this->image_dir.$product_id;
            $dir_url = $this->image_dir.$product_id;
        }

        if (!file_exists($dir)) {
            mkdir($dir, 0777, true);
        }

        $file_name = basename($url);

        if($this->is_url_exist($url)){
            $image = $this->getImage($url);
        }else{
            return '';
        }



        if(file_put_contents( $dir.'/'.$file_name,  $image)){
            return $dir_url.'/'.$file_name;
        }
        return '';
    }

    public function getWarehousesList(){

        $call = $this->call('lists');


        if(!$call or !isset($call['data']['AllWarehouses'])){
            return array();
        }

        return $call['data']['AllWarehouses'];

    }

    public function getTaxes(){

        $call = $this->call('lists');

        if(!$call or !isset($call['data']['TaxRates'])){
            return array();
        }

        return $call['data']['TaxRates'];

    }

    public function getCategories(){

        $call = $this->call('lists');

        if(!$call or !isset($call['data']['category'])){
            return array();
        }

        return $call['data']['category'];

    }

    public function getSubCategories(){

        $call = $this->call('lists');

        if(!$call or !isset($call['data']['subcategory'])){
            return array();
        }

        return $call['data']['subcategory'];

    }

    public function getBrands(){

        $call = $this->call('lists');

        if(!$call or !isset($call['data']['brands'])){
            return array();
        }

        return $call['data']['brands'];

    }

    public function getVariants(){

        $call = $this->call('lists');

        if(!$call or !isset($call['data']['AllVariants'])){
            return array();
        }

        return $call['data']['AllVariants'];

    }

    public function getCurrencies(){

        $call = $this->call('lists');

        if(!$call or !isset($call['data']['allcurrency'])){
            return array();
        }

        $data = array(
                'currencies'     =>$call['data']['allcurrency'],
                'defultcurrency' =>$call['data']['defultcurrency']
        );
        return $data;

    }



    public function getCountProducts($whid){

        $call = $this->call('getCountWarehouseProduct',array('whid'=>$whid));

        if(!$call or !isset($call['data'])){
            return 0;
        }

        return (int) $call['data'];

    }

    public function getRecentProductsCount($whid,$timestamp){

        $call = $this->call('getCountWarehouseProduct'
            ,array(
                'whid'=>$whid,
                'timestamp'=>$timestamp,
            ));

        if(!$call or !isset($call['data'])){
            return 0;
        }

        return (int) $call['data'];

    }

    public function getDefaultBiller(){

        $call = $this->call('lists');

        if(!$call or !isset($call['data']['pos_setting']['default_biller'])){
            return 0;
        }

        return (int) $call['data']['pos_setting']['default_biller'];

    }

    public function getDefaultCustomer(){

        $call = $this->call('lists');

        if(!$call or !isset($call['data']['pos_setting']['default_customer'])){
            return 1;
        }

        return (int) $call['data']['pos_setting']['default_customer'];

    }

    public function getProducts($whid,$offset,$limit, $LastModified = null){
        $call = $this->call('getProductsByWh',array(
                                                    'warehouse_id'=>$whid,
                                                    'limit'=>$limit,
                                                    'start'=>$offset,
                                                    'timestamp'=>$LastModified
                                                    ));

        if(!$call or !isset($call['data'])){
            return array();
        }

        return   $call['data'];
    }

    /**
     * [syncCustomer is used to sync the customer to the smartlife erp system]
     * @param  [array] $oc_customers [customer details]
     * @return [Integer]               [number of customer synchronized]
     */
    public function syncCustomer($oc_customers)
    {
        $id = 0 ;

        try {
               foreach ($oc_customers as $Oc_Customer) {

                 $id = $this->getCustomerByEmail($Oc_Customer['email']);

                 if($id == 0){
                    $post = array(
                                'name'=>$Oc_Customer['firstname'].' '.$Oc_Customer['lastname'],
                                'email'=>$Oc_Customer['email'],
                                'phone'=>$Oc_Customer['telephone']
                          );

                     $info =  $this->call('addcustomer', $post,'POST');
                     $id = isset($info['cid']) ? $info['cid'] : 0 ;
                 }



                }



        } catch (Exception $e) {
             return $id;
        }
        return $id;
    }


    /**
     * [getCustomerByEmail is used to get the customer details from smartlife erp system]
     * @param  string  $email
     * @return [int]                   [customer id] if exists else 0
     */

     public function getCustomerByEmail($email){

        $id = 0 ;
        try {

             $post = array( 'email'=>$email );
             $info =  $this->call('getCustomerByEmail', $post );

             $id = isset($info['data'][0]['id'])  ? $info['data'][0]['id'] : 0 ;
        } catch (Exception $e) {
             return $id ;
        }
        return $id;
     }

    /**
     * [getCustomerDetails is used to get the customer details ]
     * @param  array  $customer_details [multiple customer ids]
     * @return [array]                   [customer details]
     */
    public function getCustomerDetails($customer_details = array(), $existing = 0)
    {
        if ($customer_details) {
            $sub_query = "AND c.customer_id IN (".$customer_details.") ";
        } else {
            $sub_query = "";
        }

        if ($this->config->get('config_store_id')) {
            $store_id = $this->config->get('config_store_id');
        } else {
            $store_id = 0;
        }
        $query =  $this->db->query("SELECT *,c.customer_id,c.firstname,c.lastname, z.name AS state FROM ".DB_PREFIX."customer c LEFT JOIN ".DB_PREFIX."customer_group_description cgd ON(c.customer_group_id = cgd.customer_group_id) LEFT JOIN ".DB_PREFIX."address ad ON (c.customer_id = ad.customer_id AND c.address_id = ad.address_id) LEFT JOIN ".DB_PREFIX."zone z ON ((ad.country_id = z.country_id) AND (ad.zone_id = z.zone_id)) WHERE cgd.language_id = '" . (int)$this->config->get('config_language_id') . "' AND c.store_id = '".(int)$store_id."' ".$sub_query)->rows;

        if ($query) {
            foreach ($query as $key => $customer) {
                $sql = $this->db->query("SELECT * FROM " . DB_PREFIX . "country WHERE country_id = '".(int)$customer['country_id']."' ")->row;
                if (isset($sql['name']) && $sql['name']) {
                    $query[$key]['country_name'] = $sql['name'];
                } else {
                    $query[$key]['country_name'] = '';
                }
            }
        }
        return $query;
    }

    /**
     * [syncOrder is used to sync the order to the quick book]
     * @param  [array] $Oc_Orders [order details]
     * @return [Integer]            [number of order synchronized]
     */
    public function syncOrder($Oc_Orders)
    {
       $counter = 0;

        try {



             foreach ($Oc_Orders as  $order) {

                  $order_products = isset($order['order_products']) ? $order['order_products'] : array();
                  if (isset($order['customer_id']) && $order['customer_id']) {
                        $results = $this->getCustomerDetails($order['customer_id']);
                        if ($results) {
                            $cid  = $this->syncCustomer($results);
                            $customer_id = $cid ;
                        }
                   }

                   if(!$customer_id){
                        $customer_id = $this->getDefaultCustomer() ;
                   }


                   $DefaultBiller = $this->getDefaultBiller();
                   $warehouse     = $this->config->get('module_smartlife_warehouse');
                   $total_items   = count($order_products);
                   $product_id = array();
                   $product_type = array();
                   $product_code = array();
                   $product_name = array();
                   $real_unit_price = array();
                   $unit_price      = array();
                   $quantity        = array();
                   $product_option  = array();


                   foreach ($order_products as $product){
                        $p_id[]       = $product['product_id'];
                        $p_code[]     = $product['model'];
                        $p_type[]     = 'srandard';
                        $p_name[]     = $product['name'];
                        $r_unit_price[]  = $product['price'];
                        $u_price[]       = $product['price'];
                        $q[]         = $product['quantity'];
                        $product_option[]         = $product['option_value'];
                   }

                    $product_id        =  implode('/',$p_id);
                    $product_code      =  implode('/',$p_code);
                    $product_type      = implode('/',$p_type);
                    $product_name      =  implode('/',$p_name);
                    $real_unit_price   =  implode('/',$r_unit_price);
                    $unit_price        =  implode('/',$u_price);
                    $quantity          =  implode('/',$q);
                    $product_options   =  implode('/',$product_option);


                 $discount = isset($order['discount_total']) ? $order['discount_total'] : 0;
                 $order_tax= isset($order['tax_total']) ? $order['tax_total'] : 0;
                 $amount_paid = $order['total'];
                 $payment_status = 'paid';
                 $pos_note  = 'طلبية عن طريق المتجر' ;//$order['order_totals'];

                  $list2 = $this->call('lists2');

                 $payment_code = $this->setPaymentCode($order['payment_code']);
                 $paid_by = $payment_code;
                 $shipping = (int) $order["shipping_total"];
                 $user_id = 1 ;
                 $vwid =  $this->config->get('module_smartlife_warehouse');
                 $post = array(
                            'warehouse'=>$warehouse,
                            'customer'=>$customer_id,
                            'biller'=>$DefaultBiller,
                            'total_items'=>$total_items,
                            'product_id[]'=>$product_id,
                            'product_type[]'=>$product_type,
                            'product_code[]'=>$product_code,
                            'product_name[]'=>$product_name,
                            'real_unit_price[]'=>$real_unit_price,
                            'unit_price[]'=>$unit_price,
                            'quantity[]'=>$quantity,
                            'discount'=>$discount,
                            'order_tax'=>$order_tax,
                            'amount-paid[]'=>$amount_paid,
                            'payment_status'=>$payment_status,
                            'pos_note'=>$pos_note,
                            'balance_amount'=>0,
                            'paid_by[]'=> $payment_code,
                            'shipping'=>$shipping,
                            'user_id'=>$user_id,
                            'reference_no'=>'EC'.$order['order_id'],
                            'pos_add'=>3,
                            'product_option[]'=>$product_options,
                            'vwid'=>$vwid
                 );

             /*print_r($post);
             exit();*/
             $info =  $this->call('sale2', $post,'POST');

              break;
            }

        } catch (Exception $e) {
            return $counter;
        }

        return $counter;
    }

    public function setPaymentCode($code){

        $list2 = $this->call('lists2');
        $payments = $list2['data']['payment'];


         foreach ($payments as $payment){
             if($payment['type'] == $code){
                return $payment['value'];
            }
        }

        foreach ($payments as $payment){
             if($payment['type'] == 'cash'){

                return $payment['value'];
            }
        }
    }


}
